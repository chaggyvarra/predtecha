<?php
session_start();
require_once '../vendor/autoload.php';
include("../data/Settings.php");
require '../vendor/smarty/smarty/libs/Smarty.class.php';

$smarty = new Smarty;




//смотрим где находимся
$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url = ltrim($url, "/");
$url = rtrim($url, "/");
$pages = explode("/", $url);


//$isAdminPage = $pages[0] != "admin" ? false : true;

$mainComponent = "home";
$component = !empty($pages[1]) ? "components/".$pages[1].".php" : "components/".$mainComponent.".php";

if (file_exists($component) == false)
    $component = "components/".$mainComponent.".php";

$action = $pages[2];

include ($component);

$smarty->cache_lifetime = 1;

include("templates/common/header.php");
include("templates/common/topside.php");

$smarty->display($pageTemplate);
//include("templates/home/{$__component}.php");

include("templates/common/footer.php");