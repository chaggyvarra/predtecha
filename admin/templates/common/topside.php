<!-- Header -->
<header id="js-header" class="u-header u-header--sticky-top">
<div class="u-header__section u-header__section--admin-dark g-min-height-65">
<nav class="navbar no-gutters g-pa-0">
<div class="col-auto d-flex flex-nowrap u-header-logo-toggler g-py-12">
    <!-- Logo -->
    <a href="index.php" class="navbar-brand d-flex align-self-center g-hidden-xs-down g-line-height-1 py-0 g-mt-5"></a>
    <!-- End Logo -->

    <!-- Sidebar Toggler -->
    <a class="js-side-nav u-header__nav-toggler d-flex align-self-center ml-auto" href="javascript: void();" data-hssm-class="u-side-nav--mini u-sidebar-navigation-v1--mini" data-hssm-body-class="u-side-nav-mini" data-hssm-is-close-all-except-this="true" data-hssm-target="#sideNav">
        <i class="hs-admin-align-left"></i>
    </a>
    <!-- End Sidebar Toggler -->
</div>

<!-- Messages/Notifications/Top Search Bar/Top User -->
<div class="col-auto d-flex g-py-12 g-pl-40--lg ml-auto">

<!-- Top User -->
<div class="col-auto d-flex g-pt-5 g-pt-0--sm g-pl-10 g-pl-20--sm">
    <div class="g-pos-rel g-px-10--lg">
        <a id="profileMenuInvoker" class="d-block" href="javascript: void();" aria-controls="profileMenu" aria-haspopup="true" aria-expanded="false" data-dropdown-event="click" data-dropdown-target="#profileMenu" data-dropdown-type="css-animation" data-dropdown-duration="300"
           data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
                <span class="g-pos-rel">
        <span class="u-badge-v2--xs u-badge--top-right g-hidden-sm-up g-bg-secondary g-mr-5"></span>
                <img class="g-width-30 g-width-40--md g-height-30 g-height-40--md rounded-circle g-mr-10--sm" src="/admin/assets/img-temp/130x130/img1.jpg" alt="Image description">
                </span>
                <span class="g-pos-rel g-top-2">
        <span class="g-hidden-sm-down">Администратор</span>
                <i class="hs-admin-angle-down g-pos-rel g-top-2 g-ml-10"></i>
                </span>
        </a>

        <!-- Top User Menu -->
        <ul id="profileMenu" class="g-pos-abs g-left-0 g-width-100x--lg g-nowrap g-font-size-14 g-py-20 g-mt-17 rounded" aria-labelledby="profileMenuInvoker">
            <li class="g-hidden-sm-up g-mb-10">
                <a class="media g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-pos-rel g-mr-12">
          <span class="u-badge-v1 g-top-minus-3 g-right-minus-3 g-width-18 g-height-18 g-bg-secondary g-font-size-10 g-color-white rounded-circle p-0">10</span>
                    <i class="hs-admin-comment-alt"></i>
                    </span>
                    <span class="media-body align-self-center">Unread Messages</span>
                </a>
            </li>
            <li class="g-hidden-sm-up g-mb-10">
                <a class="media g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-bell"></i>
        </span>
                    <span class="media-body align-self-center">Notifications</span>
                </a>
            </li>
            <li class="g-mb-10">
                <a class="media g-color-primary--hover g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-user"></i>
        </span>
                    <span class="media-body align-self-center">My Profile</span>
                </a>
            </li>
            <li class="g-mb-10">
                <a class="media g-color-primary--hover g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-rocket"></i>
        </span>
                    <span class="media-body align-self-center">Upgrade Plan</span>
                </a>
            </li>
            <li class="g-mb-10">
                <a class="media g-color-primary--hover g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-layout-grid-2"></i>
        </span>
                    <span class="media-body align-self-center">Latest Projects</span>
                </a>
            </li>
            <li class="g-mb-10">
                <a class="media g-color-primary--hover g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-headphone-alt"></i>
        </span>
                    <span class="media-body align-self-center">Get Support</span>
                </a>
            </li>
            <li class="mb-0">
                <a class="media g-color-primary--hover g-py-5 g-px-20" href="#!">
                    <span class="d-flex align-self-center g-mr-12">
          <i class="hs-admin-shift-right"></i>
        </span>
                    <span class="media-body align-self-center">Sign Out</span>
                </a>
            </li>
        </ul>
        <!-- End Top User Menu -->
    </div>
</div>
<!-- End Top User -->
</div>
<!-- End Messages/Notifications/Top Search Bar/Top User -->


</nav>
</div>
</header>
<!-- End Header -->


<main class="container-fluid px-0 g-pt-65">
<div class="row no-gutters g-pos-rel g-overflow-x-hidden">
<?php
include ("sidemenu.php");
?>
    <div class="col g-ml-45 g-ml-0--lg g-pb-65--md">
        <div class="g-pa-20">
