<!-- Sidebar Nav -->
<div id="sideNav" class="col-auto u-sidebar-navigation-v1 u-sidebar-navigation--dark">
    <ul id="sideNavMenu" class="u-sidebar-navigation-v1-menu u-side-nav--top-level-menu g-min-height-100vh mb-0">
        <!-- News -->
        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--has-sub-menu u-side-nav--top-level-menu-item">
            <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="javascript: void();" data-hssm-target="#subMenu1">
              <span class="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
       <i class="hs-admin-layout-list-thumb"></i>
    </span>
                <span class="media-body align-self-center">Новости</span>
              <span class="d-flex align-self-center u-side-nav--control-icon">
      <i class="hs-admin-angle-right"></i>
    </span>
                <span class="u-side-nav--has-sub-menu__indicator"></span>
            </a>

            <!-- News: Submenu-1 -->
            <ul id="subMenu1" class="u-sidebar-navigation-v1-menu u-side-nav--second-level-menu mb-0">
                <!-- Dashboards v1 -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/news/add">
                        <span class="media-body align-self-center">Добавить новость</span>
                    </a>
                </li>
                <!-- End News v1 -->

                <!-- News v2 -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/news/list">
                        <span class="media-body align-self-center">Список новостей</span>
                    </a>
                </li>
                <!-- End News v2 -->
            </ul>
            <!-- End News: Submenu-1 -->
        </li>
        <!-- End News -->

        <!-- Layouts Settings -->
        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--has-sub-menu u-side-nav--top-level-menu-item">
            <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="javascript: void();" data-hssm-target="#subMenu2">
              <span class="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
      <i class="hs-admin-calendar"></i>
    </span>
                <span class="media-body align-self-center">Расписание богослужений</span>
              <span class="d-flex align-self-center u-side-nav--control-icon">
      <i class="hs-admin-angle-right"></i>
    </span>
                <span class="u-side-nav--has-sub-menu__indicator"></span>
            </a>

            <!-- Layouts Settings: Submenu-1 -->
            <ul id="subMenu2" class="u-sidebar-navigation-v1-menu u-side-nav--second-level-menu mb-0">
                <!-- Header Static -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/schedule/add">
                        <span class="media-body align-self-center">Добавить расписание на месяц</span>
                    </a>
                </li>
                <!-- End Header Static -->

                <!-- Hide Sidebar -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/schedule/list">
                        <span class="media-body align-self-center">Список</span>
                    </a>
                </li>
                <!-- End Hide Sidebar -->
            </ul>
            <!-- End Layouts Settings: Submenu-1 -->
        </li>
        <!-- End Layouts Settings -->

        <!-- App Views -->
        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--has-sub-menu u-side-nav--top-level-menu-item">
            <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="javascript: void();" data-hssm-target="#subMenu4">
              <span class="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
      <i class="hs-admin-gallery"></i>
    </span>
                <span class="media-body align-self-center">Фотогалерея</span>
              <span class="d-flex align-self-center u-side-nav--control-icon">
      <i class="hs-admin-angle-right"></i>
    </span>

                <span class="u-side-nav--has-sub-menu__indicator"></span>
            </a>

            <!-- App Views: Submenu-1 -->
            <ul id="subMenu4" class="u-sidebar-navigation-v1-menu u-side-nav--second-level-menu mb-0">
                <!-- Projects -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/gallery/add">
                        <span class="media-body align-self-center">Добавить фото</span>
                    </a>
                </li>
                <!-- End Projects -->

                <!-- Chat -->
                <li class="u-sidebar-navigation-v1-menu-item u-side-nav--second-level-menu-item">
                    <a class="media u-side-nav--second-level-menu-link g-px-15 g-py-12" href="../admin/gallery/list">
                        <span class="media-body align-self-center">Список фото</span>
                    </a>
                </li>
                <!-- End Chat -->
            </ul>
            <!-- End App Views: Submenu-1 -->
        </li>
        <!-- End App Views -->

        <!-- Metrics -->
        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
            <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="../admin/contacts/list">
              <span class="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
      <i class="hs-admin-map"></i>
    </span>
                <span class="media-body align-self-center">Реквизиты и контакты</span>
            </a>
        </li>
        <!-- End Metrics -->

    </ul>
</div>
<!-- End Sidebar Nav -->