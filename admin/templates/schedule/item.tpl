
{if $action == "add"}
    <h2>Внесение новой записи в расписание богослужений</h2>
<form>
<div class="row">
    <div class="col-md-8 g-pa-3">
        <div class="g-mb-30">
            <div class="row">
                <div class="col-md-4">
                    <!-- Default Input -->
                    <div class="form-group g-mb-0">
                        <label class="g-mb-10" for="inputGroup-1_1">Месяц</label>

                        <div class="g-pos-rel">
                      <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
	                  	<i class="hs-admin-check g-absolute-centered g-font-size-default g-color-secondary"></i>
	                	</span>
                            <input id="month" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="number" placeholder="Номер месяца" min="1" max="12">
                        </div>
                    </div>
                    <!-- End Default Input -->
                </div>
                <div class="col-md-4">
                    <!-- Default Input -->
                    <div class="form-group g-mb-0">
                        <label class="g-mb-10" for="inputGroup-1_1">День</label>

                        <div class="g-pos-rel">
                      <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
	                  	<i class="hs-admin-check g-absolute-centered g-font-size-default g-color-secondary"></i>
	                	</span>
                            <input id="day" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="number" placeholder="Номер дня" min="1" max="31">
                        </div>
                    </div>
                    <!-- End Default Input -->
                </div>
                <div class="col-md-4">
                    <!-- Default Input -->
                    <div class="form-group g-mb-0">
                        <label class="g-mb-10" for="inputGroup-1_1">Время</label>

                        <div class="g-pos-rel">
                      <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
	                  	<i class="hs-admin-check g-absolute-centered g-font-size-default g-color-secondary"></i>
	                	</span>
                            <input id="day" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="time" placeholder="Время">
                        </div>
                    </div>
                    <!-- End Default Input -->
                </div>
            </div>
            <small class="form-text text-muted g-font-size-11 g-mt-10 g-mb-30">
                <strong>Note:</strong> для изменения значения поставьте курсор в поле и стрелками ↑ и ↓ измените до нужного значения; либо введите нужную цифру самостоятельно.
            </small>

            <!-- Default Input -->
            <div class="form-group g-mb-30">
                <label class="g-mb-10" for="inputGroup-1_1">Колонка 1</label>

                <textarea id="col1" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" rows="3" placeholder="Текст для колонки 1"></textarea>
                <small class="form-text text-muted g-font-size-11 g-mt-10">
                    <strong>Note:</strong> если вы хотите перенести строку, то поставьте тег <code>&#60;br&#62;</code> перед переносимой строкой.
                </small>
            </div>
            <!-- End Default Input -->

            <!-- Default Input -->
            <div class="form-group g-mb-30">
                <label class="g-mb-10" for="inputGroup-1_1">Колонка 2</label>

                <textarea id="col2" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" rows="3" placeholder="Текст для колонки 2"></textarea>
            </div>
            <!-- End Default Input -->

    </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
</div>
</form>

{elseif $action == "edit"}

{/if}





