<?php

// API endpoint to handle image uploads and resizing
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Define the target directory where the resized images will be saved
    $targetDirectory = 'path/to/target/folder/';

    // Check if the target directory exists, create it if it doesn't
    if (!is_dir($targetDirectory)) {
        mkdir($targetDirectory, 0777, true);
    }

    // Check if an image file was uploaded
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
        // Get the uploaded file details
        $uploadedFile = $_FILES['image'];
        $tempFilePath = $uploadedFile['tmp_name'];
        $originalFileName = basename($uploadedFile['name']);

        // Generate a unique filename for the resized image
        $resizedFileName = uniqid('resized_') . '_' . $originalFileName;

        // Specify the desired dimensions for the resized image
        $targetWidth = 800;
        $targetHeight = 600;

        // Create a new image from the uploaded file
        $sourceImage = imagecreatefromstring(file_get_contents($tempFilePath));

        // Get the original image's width and height
        $sourceWidth = imagesx($sourceImage);
        $sourceHeight = imagesy($sourceImage);

        // Calculate the aspect ratio of the original image
        $aspectRatio = $sourceWidth / $sourceHeight;

        // Calculate the new dimensions while preserving the aspect ratio
        if ($targetWidth / $targetHeight > $aspectRatio) {
            $newWidth = $targetHeight * $aspectRatio;
            $newHeight = $targetHeight;
        } else {
            $newWidth = $targetWidth;
            $newHeight = $targetWidth / $aspectRatio;
        }

        // Create a new blank image with the desired dimensions
        $resizedImage = imagecreatetruecolor($targetWidth, $targetHeight);

        // Resize the original image to fit the new dimensions
        imagecopyresampled(
            $resizedImage,
            $sourceImage,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $sourceWidth,
            $sourceHeight
        );

        // Save the resized image to the target directory
        $targetFilePath = $targetDirectory . $resizedFileName;
        imagejpeg($resizedImage, $targetFilePath, 80); // Adjust the image quality (80) as needed

        // Clean up memory
        imagedestroy($sourceImage);
        imagedestroy($resizedImage);

        // Send a JSON response with the path to the resized image
        $response = ['path' => $targetFilePath];
        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        // Send an error response if no image file was uploaded
        http_response_code(400);
        echo 'No image file uploaded.';
    }
} else {
    // Send an error response for unsupported HTTP methods
    http_response_code(405);
    echo 'Method Not Allowed.';
}

