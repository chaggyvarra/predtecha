<?php
namespace Church\Utils;

use Church\Db\Database;

class Checkers {
    protected $db;

    public function __construct() {
        $this->db = new Database();
    }

    public function checkEmail($email) {
        $lQuery = $this->db->query("select * from `users` where `login` = '$email' limit 0,10", "select");
        $eQuery = $this->db->query("select * from `users` where `email` = '$email' limit 0,10", "select");

        if (count($lQuery) > 0 && count($eQuery) > 0) {

            return true;
        }

        return false;
    }

    public function checkSid($sid) {
        $sQuery = $this->db->query("select * from `users` where `sid` = '$sid' limit 0,10", "select");
        if (count($sQuery) > 0) {

            return false;
        }

        return true;
    }
    public static function checkPassword($hash, $password) {
        $full_salt = substr($hash, 0, 29);
        $new_hash = crypt($password, $full_salt);
        return ($hash == $new_hash);
    }
}