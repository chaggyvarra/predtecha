<?php
const PAGE_TITLE = "Храм Рождества Иоанна Предтечи";
const PAGE_DESCRIPTION = "";
const PAGE_KEYWORDS = "";
const PAGE_ROBOTS = "all, follow";

const HOMEPAGE = "home";

const YANDEX_SHOPID = "563094";
const YANDEX_SCID = "897421";

const FB = "https://www.facebook.com/ioannopredtechensky";
const VK = "https://vk.com/public175894929";
const INSTAGRAM = "https://www.instagram.com/hram_predtecha/";
const YOUTUBE = "https://www.youtube.com/channel/UCKbj_lLAxt3HOHaAMTrnf9A";
const ZEN = "https://zen.yandex.ru/id/623b55bacd98441a8442ed35";
const TG = "https://t.me/hram_predtecha";

const DEFAULT_PICTURE = "/assets/default.jpg";
const UPLOADS_DIR = "/uploads/";

$__CALENDAR_SOURCES = array(
    0 => "http://www.holytrinityorthodox.com/ru/calendar/calendar.php?dt=1&header=1&lives=5&trp=1&scripture=1",
    1 => "http://script.days.ru/php.php?para=1&feofan=1&encoding=u&name=0&trop=0&icon=0"
);

