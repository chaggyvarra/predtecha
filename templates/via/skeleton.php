<div class="container g-mt-100">
<h1>Вокальный ансамбль «Елей»</h1>
    <p>Вокальный ансамбль, под руководством Ларисы Язовой, образован
        из певчих клиросного хора храма св. великомученика и целителя
        Пантелеимона. Существует с мая 2014г. Осуществляет
        благотворительную концертную деятельность, участвует в приходских
        и городских концертах. Репертуар включает песни современных
        авторов, духовные канты, песни на стихи русских поэтов, иностранные
        произведения, рождественские песнопения разных народов, русские
        народные песни.</p>

<div class="row">
    <?php
    $dir = opendir('./assets/pic/via/');
    $count = 0;
    while($file = readdir($dir)){
        if($file == '.' || $file == '..' || is_dir('./assets/pic/via/' . $file)){
            continue;
        }
        $count++;
    }
    //$count = 20;
    if ($count > 0) {
        $i = 1;
        do {
            echo "<div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/via/$i.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/via/thumb/$i.jpg'>
            </a>
        </div>
    </div>";
            $i++;
        }
        while ($i <= $count);
    }
    ?>
</div>

</div>