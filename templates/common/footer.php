<!-- Footer -->
<footer class="container g-pt-100 g-pb-70">
    <div class="row">
        <div class="col-md-2 text-center text-md-left g-mb-30">
            <!-- Logo -->
            <a class="d-inline-block g-text-underline--none--hover mr-4 mb-4" href="/">
                <img class="g-width-95" src="/assets/img/logo/logoBig.png" alt="Logo">
            </a>
            <!-- End Logo -->
        </div>

        <div class="col-md-7 text-center g-mb-30">
            <span class="d-block g-color-gray-dark-v5 g-font-size-13 mb-4">&copy; Храм Рождества Иоанна Предтечи <? echo date('Y');?></span>

            <!-- Footer - List -->
            <ul class="list-inline mb-5">
                <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/gallery/">Фотогалерея</a>
                </li>
                <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="//www.p-blagovest.ru/arhiv/" target="_blank">Наша газета</a>
                </li>
                <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/charity/">Пожертвования</a>
                </li>
                <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/requests/">Заказать Богослужение</a>
                </li>
                <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/contacts/">Контакты</a>
                </li>
                <!--              <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/uploads/blagoslovenie-na-velikiy-post.pdf" target="_blank">БЛАГОСЛОВЕНИЕ НА ВЕЛИКИЙ ПОСТ</a>
                              </li>
                              <li class="list-inline-item mx-3"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-weight-500" href="/order-worship/">Заказать Богослужение</a>
                </li>-->
            </ul>
            <!-- End Footer - List -->

            <!-- Go to Top -->
            <a class="js-go-to u-go-to-v3 g-width-40 g-height-40 g-color-primary" href="#" data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
                <i class="g-brd-gray-light-v3 g-brd-primary--hover rounded-0 fa fa-long-arrow-up"></i>
            </a>
            <!-- End Go to Top -->
        </div>

        <div class="col-md-3 text-center g-mb-30">
            <h3 class="g-font-weight-600 g-font-size-13 text-uppercase">Подписывайтесь на нас в соцсетях:</h3>

            <!-- Social Icons -->
            <ul class="list-inline mb-0">
                <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-gray-dark-v5 g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-13" href="<?php echo TG;?>" target="_blank">
                        <i class="fa fa-telegram"></i>
                    </a>
                </li>
                <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-gray-dark-v5 g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-13" href="<?php echo VK;?>" target="_blank">
                        <i class="fa fa-vk"></i>
                    </a>
                </li>
                <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-gray-dark-v5 g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-13" href="<?php echo ZEN;?>" target="_blank">
                        <i class="fa fa-hacker-news"></i>
                    </a>
                </li>
                <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-gray-dark-v5 g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-13" href="<?php echo YOUTUBE;?>" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
            </ul>
            <!-- End Social Icons -->


        </div>
    </div>
</footer>
<!-- End Footer -->


</main>

<!-- JS Global Compulsory -->
<script src="/assets/vendor/jquery/jquery.min.js"></script>
<script src="/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/assets/vendor/popper.min.js"></script>
<script src="/assets/vendor/bootstrap/bootstrap.min.js"></script>

<script src="/assets/vendor/bootstrap/offcanvas.js"></script>

<!-- JS Unify -->
<script src="/assets/js/hs.core.js"></script>
<script src="/assets/js/custom.js"></script>

<!-- JS Implementing Plugins -->
<script src="/assets/vendor/chosen/chosen.jquery.js"></script>

<!-- JS Unify -->
<script src="/assets/js/components/hs.select.js"></script>

<script src="/assets/js/components/hs.header.js"></script>
<script src="/assets/js/helpers/hs.hamburgers.js"></script>

<script src="/assets/js/helpers/hs.navigation-splitted.js"></script>

<script src="/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

<script src="/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
<script  src="/assets/js/components/hs.popup.js"></script>
<script  src="/assets/js/components/jquery.datetimepicker.js"></script>

<script src="/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>

<script  src="/assets/js/components/hs.count-qty.js"></script>

<!-- JS Implementing Plugins -->
<script  src="/assets/vendor/custombox/custombox.min.js"></script>

<!-- JS Unify -->
<script  src="/assets/js/components/hs.modal-window.js"></script>


<script src="/assets/js/components/hs.go-to.js"></script>

<!-- JS Custom -->
<script src="/assets/js/custom.js"></script>

<!-- JS Global Compulsory -->
<script src="/assets/vendor/popper.min.js"></script>

<script>
    $(document).on('ready', function () {
        // initialization of custom select
        $.HSCore.components.HSSelect.init('.js-custom-select');

        // initialization of forms
        $.HSCore.components.HSCountQty.init('.js-quantity');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of popups
        $.HSCore.components.HSModalWindow.init('[data-modal-target]');

        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of HSDropdown component
        $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
            afterOpen: function(){
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of HSScrollBar component
        $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

        // initialization of popups
        $.HSCore.components.HSPopup.init('.js-fancybox');

    });

    $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });
    });

    $('#calendar').datetimepicker({
        date: new Date(),
        viewMode: 'YMD',
        language: 'ru',
        onDateChange: function(){

            var scrollID = this.getText('MM-DD');
            var scrollTop = $('#' + scrollID).offset().top;
            var scrollTo = scrollTop - 100;
            $(document).scrollTop(scrollTo);
        }
    });
</script>
</body>
</html>