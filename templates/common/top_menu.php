<!-- Header -->
<header id="js-header" class="u-header u-header--sticky-top u-header--toggle-section u-header--change-appearance" data-header-fix-moment="300">
    <div class="u-header__section u-header__section--light g-bg-white-opacity-0_7 g-transition-0_3 g-py-10" data-header-fix-moment-exclude="g-py-10" data-header-fix-moment-classes="u-shadow-v18 g-py-0">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Logo -->
                <a href="/" class="navbar-brand">
                    <img src="/assets/img/logo/logoFull.png" alt="Image Description">
                </a>
                <!-- End Logo -->

                <!-- Navigation -->
                <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
                    <ul class="navbar-nav ml-auto text-uppercase g-font-weight-600">
                        <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                            <a href="/" class="nav-link px-0">Главная</a>
                        </li>
                        <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                            <a href="/news/" class="nav-link px-0">Новости

                            </a>
                        </li>
                        <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                            <a href="/service-schedule/" class="nav-link px-0">Богослужения
                            </a>
                        </li>
                        <li class="nav-item g-mx-20--lg dropdown">
                            <a href="" class="nav-link dropdown-toggle g-px-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">О храме </a>
                            <ul class="dropdown-menu rounded-0 g-text-transform-none g-brd-none g-brd-top g-brd-primary g-brd-top-2 g-mt-20 g-mt-10--lg--scrolling">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/history/">История</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/novadays/">Строительство храма</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/saints/">Святые</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/holidays/">Наши праздники</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/video/">Фильм о храме</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/contacts/">Контакты</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item g-mx-20--lg dropdown">
                            <a href="" class="nav-link dropdown-toggle g-px-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">О приходе </a>
                            <ul class="dropdown-menu rounded-0 g-text-transform-none g-brd-none g-brd-top g-brd-primary g-brd-top-2 g-mt-20 g-mt-10--lg--scrolling">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/shrines/">Святыни</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/prior/">Настоятель</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/museum/">Музей</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/crafts/">Ремесленные мастерские</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/goat-farm/">Козья ферма</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/via/">Вокальный ансамбль «Елей»</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="/gallery/">Фотогалерея</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item g-ml-20--lg g-mr-0--lg">
                            <a href="/requests/" class="nav-link px-0">Требы онлайн
                            </a>
                        </li>
                        <li class="nav-item g-ml-20--lg g-mr-0--lg">
                            <a href="/charity/" class="btn btn-sm btn-success">Помочь храму</a>
                        </li>
                    </ul>
                </div>
                <!-- End Navigation -->
            </div>
        </nav>
    </div>
</header>
<!-- End Header -->