<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Title -->
    <title><?php echo $meta['title']; ?></title>
    <meta name="yandex-verification" content="a19ea3698a5b363a" />

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.png">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/offcanvas.css">

    <link rel="stylesheet" href="/assets/vendor/icon-awesome/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Alice" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- CSS Unify -->
    <link rel="stylesheet" href="/assets/css/unify-core.css">
    <link rel="stylesheet" href="/assets/css/unify-components.css">
    <link rel="stylesheet" href="/assets/css/unify-globals.css">
    <link rel="stylesheet" href="/assets/vendor/animate.css">
    <link rel="stylesheet" href="/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link  rel="stylesheet" href="/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/assets/vendor/fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/datapicker/jquery.datetimepicker.css"/>

    <link  rel="stylesheet" href="/assets/vendor/animate.css">
    <link  rel="stylesheet" href="/assets/vendor/custombox/custombox.min.css">

    <link  rel="stylesheet" href="/assets/vendor/slick-carousel/slick/slick.css">
    <link  rel="stylesheet" href="/assets/vendor/jquery-ui/themes/base/jquery-ui.min.css">

    <link rel="stylesheet" href="/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/assets/css/custom.css">
    <!--[if lt IE 9]>
    <script>
        document.createElement('figure');
        document.createElement('figcaption');</script>
    <![endif]-->
</head>

<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(54461665, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54461665" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<main>