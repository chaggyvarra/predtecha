<div class="container g-mt-100">
    <div class="row">
        <div class="col-md-3">
            <h3>Фотогалерея</h3>
            <div class="row">
                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/1.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/2.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/3.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/4.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/5.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/5.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/6.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/6.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/7.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/7.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/8.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/8.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/9.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/9.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/10.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/10.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/11.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/11.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/history/12.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/history/thumb/12.jpg" alt="example-image">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <h1>История храма Рождества Иоанна Предтечи</h1>
            <figure class="sign-right">
                <p><img src="/assets/pic/photogallery/0.jpg" width="250"></p>
                <figcaption>Каменный храм в честь Рождества Иоанна Предтечи, разрушенный в 40-е годы XX века.</figcaption>
            </figure>
            <p>
                Вблизи озера Глушица через Москву-реку пролегал древнейший путь, освоенный, судя по остаткам городищ, еще в I тысячелетии до н. э. После присоединения Коломны к Московскому княжеству он становится одним из важнейших в экономическом и стратегическом отношении. Этот путь стал называться Астраханским трактом. В 1581 г., после смерти сына, Иван Грозный повелел поставить здесь монастырь в честь Рождества Иоанна Предтечи и устроить переправу через реку. Монахами в новом монастыре стали бывшие опричники. В писчей книге Приказа большого двора за 1631-1633 гг. указано: «За озером Здегошем, на монастырской земле стоял храм Рождества Иоанна Предтечи, храм великомученицы Параскевы, нарицаемой Пятницы, да придел Николая Чудотворца, по прошествии времени монастырь от разорения запустел». Заброшенные монастырские земли были переданы Патриарху Филарету и вскоре перешли в Дворцовое ведомство. На месте монастыря возникло село Новорождествено. Ни монастырь, ни его старые храмы не были восстановлены. В селе заново отстроили деревянный храм в честь Рождества Иоанна Предтечи. В 1710 г. Петр I подарил село Новорождествено и Раменскую волость графу И.А.Мусину-Пушкину в награду за подавление восстания в Астрахани в 1722 г., пожаловав его в вечное владение. В 1730 г. помещик возвел двухэтажную каменную церковь в честь Рождества Иоанна Предтечи, нижний придел которой освятили во имя святителя Николая. Имелось богатейшее мраморное Распятие. Среди многих старинных икон особо почиталась древняя Новодворская икона Божией Матери, в серебряной ризе. Раньше она принадлежала старинному роду князей Хромых-Волконских-Орловых. Церковный хор славился в округе. В 1852 г. село стало самым крупным в имении князей Волконских. После революции 1917 г. село Новорождествено переименовали в Новое. В 1938 г. церковь закрыли, решив разобрать, под тем предлогом, что для строительства новой школы нужен кирпич.<br><br>
                Настоятеля храма престарелого отца Феодора Богословского, отца Петра Озерецковского, арестовали, «Тройка» (орган административной внесудебной репрессии)  вынесла приговор о расстреле на Бутовском полигоне. Так же после ареста, пономарь Дмитрий Ильинский, ключницы Ольга  Евдокимова и Надежда Кислова,  были отправлены на строительство Беломорканала, откуда уже не вернулись.<br><br>
                 В 1940 г. оставшуюся часть церкви и колокольни взорвали, территорию разровняли тракторами и засыпали песком. А со временем и от села Новорождествено не осталось ни названия, ни храма Рождества Иоанна Предтечи, ни пяти часовен, что стояли на каждой из улиц. Когда возник город Жуковский, на его окраине расположилось городское общество охотников и рыболовов, которые установили на месте храма Рождества Иоанна Предтечи стенды для стрельбы по движущимся мишеням.<br><br>
                7 июля 1997 года, состоялся крестный ход, на месте разрушенного храма  был установлен и освящен крест, впервые за 60 лет отслужены молебен и панихида. Праздничный молебен и панихиду отслужил настоятель храма Св. Архистратига Михаила  г.Жуковского отец
                Николай Булгаков. Памятный крест был изготовлен в одной из мастерских ЛИИ.<br><br>
                 В июле 1998 г. мэрия города Жуковского дала разрешение на археологические работы для определения местонахождения храма Рождества Иоанна Предтечи. Вскоре обнаружились пол, солея, амвон и стены храма. Было найдено и место престола.<br><br>
                Раскопками на месте разрушенного Предтеченского храма, строительством нового храма, деревянного, на месте открывшегося старого основания, взялся настоятель храма св. Пантелеймона города Жуковского протоиерей Николай Струков со своим приходом. Архитектор Е.Константинов выполнил проект деревянного храма, и уже 19 декабря 1998 г., в праздник святителя Николая Чудотворца, после 60-летнего перерыва совершилась первая Божественная литургия в строящемся деревянном храме.<br><br>
                В1999—2003 гг. на его территории построены: трапезная, колодец, сторожка, историко-археологическая избушка. В марте 2004 года по инициативе Жуковского отделения Всероссийской общественной организации «Боевое братство» под руководством Ивана Смирнова к востоку от храма была посажена Аллея памяти в честь воинов, погибших в Афганистане, Чечне, Беслане, а также сотрудников правоохранительных органов, убитых при исполнении служебных обязанностей в мирное время. Активное участие в увековечении памяти героев принимали их родственники. С тех пор они собираются здесь вместе с однополчанами погибших.  В памятные даты члены «Боевого братства» привозят сюда воспитанников военно-патриотических клубов, учащихся школ и лицеев – для того, чтобы почтить память погибших героев Отечества и провести уроки мужества, посвящённые доблестным примерам их боевых подвигов. К западу от храма в честь Рождества Иоанна Предтечи находится старинное кладбище. Надписи на его могильных плитах свидетельствуют о захоронениях XVIII века. Кладбище функционировало до 1952 года. Во время Великой Отечественной войны и после её окончания здесь были похоронены умершие от ран в госпиталях и погибшие при испытаниях самолётов лётчики 45-й Авиационной дивизии дальнего действия, базировавшейся на аэродроме «Раменское». В октябре 2013 года на месте их погребения установили памятник и отслужили литию. Многие люди приезжают на приход Иоанно-Предтеченского храма, чтобы поклониться святому месту и узнать о его прошлом. Для них проводят экскурсии, показывают документальный фильм об истории села Новорождествено.  В октябре 2006 года было получено благословение митрополита Крутицкого и Коломенского Ювеналия на строительство храма во имя новомученников Жуковских (Новорождественских) рядом с Иоанно-Предтеченским. Вслед за этим выбрали место для будущей церкви, расчистили площадку для её строительства. В настоящее время ведётся проектирование будущего храма, а на месте его возведения идут подготовительные работы.<br><br>
                Ныне настоятелем Иоанно – Предтеченского храма является иерей Сергий Симаков.
            </p>
        </div>
    </div>

</div>
