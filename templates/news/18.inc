<h2>Военно-патриотическое мероприятие «Чтобы помнили…» в Жуковском</h2>
<p>21 и 22 июня в канун 78-й годовщины начала Великой Отечественной войны на приходе Иоанно-Предтеченского храма г.Жуковского прошло военно-патриотическое мероприятие «Чтобы помнили…». Мероприятие проводилось в рамках реализации государственной программы «Патриотическое воспитание граждан РФ на 2016-2020 гг» и проекта «Мы с Россией вместе – душа на месте!» по духовно-нравственному и патриотическому воспитанию молодежи БФ «Наследие Отечества» и ФА по делам Молодежи.</p>
<p>Организаторами мероприятия являлись БФ «Наследие Отечества», АНО «Подвиг», РОСПАТРИОТ центр, ФА «Росмолодежь», приходы Пантелеимоновского и Иоанно-Предтеченского храмов.</p>
<p>45 участников мероприятия в возрасте от 12 до 17 лет вместе с учителями и родителями прибыли к Иоанно-Предтеченскому храму, где разбили лагерь и установили палатки для ночлега. Настоятель храма священник Сергий Симаков отслужил молебен на начало доброго дела, окропил участников святой водой, а затем отслужил литию в память погибших на полях ВОВ и сказал напутственное слово. Затем участников приветствовала президент БФ «Наследие Отечества» Надежда Буракова. Ребята отправились в ров, расположенный на территории прихода, где разбились на 2 команды и в течение 2-х часов провели 4 раунда игры «Лазертаг». Игру оживлял рев двигателей самолетов, готовящихся к проведению авиасалона МАКС – 2019 расположенного рядом аэродрома ЛИИ. После ужина ребята собрались у костра, пекли картошку, а помощник настоятеля Дмитрий Язов рассказал им историю Иоанно-Предтеченского храма.</p>
<p>Затем состоялся просмотр фильма «Брестская Крепость», а около полуночи ребята вышли на берег Москвы-реки и пустили по воде зажженные свечи в память о погибших во время ВОВ. С утра после завтрака ребят ожидали страйк-бол и историческая реконструкция, где они смогли подержать и разобрать настоящее оружие времен войны. Завершилось мероприятие на Аллее памяти воинам-интернационалистам напутственным словом руководителя АНО «Подвиг» Ивана Бондарева.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/145.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/145.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/146.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/146.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/147.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/147.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/148.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/148.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/149.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/149.jpg'>
            </a>
        </div>
    </div>


</div>