<h2>Празднование Казанской иконы Божией Матери</h2>
<p><i>«Заступнице усердная, Мати Господа Вышняго, за всех молиши Сына Твоего Христа Бога нашего... всем полезная даруй и вся спаси, Богородице Дево: Ты бо ecи Божественный покров рабом Твоим». (Тропарь, глас 4-й).</i><br><br>
    4 ноября церковь празднует память Казанской иконы Божией Матери.
    На протяжении нескольких столетий от образа Своего, явленного в городе Казани, Царица Небесная щедро раздает Свою благодатную помощь всем православным, с верою и надеждой к Ее защите прибегающим.
    Из всех почитаемых икон Богородицы, в Русской Православной Церкви, ни одна так не распространена в таком количестве списков, как Казанская.<br><br>
    В этот теплый праздник надежды, в Иоанно — Предтеченском храме, на утрене совершалось акафистное прославление Божией Матери перед Её иконой "Казанская". После окончания прославления состоялась праздничная Литургия, где прихожане храма и паломники приобщились Святых Христовых тайн.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/190.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/190.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/191.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/191.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/192.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/192.jpg'>
            </a>
        </div>
    </div>

</div>