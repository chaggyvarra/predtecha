<h2>Воздвижение Честного и Животворящего Креста Господня.</h2>
<p>27 сентября 2019 года в Иоанно – Предтеченском храме совершением Божественной литургии встретили двунадесятый праздник Воздвижения Честного Животворящего Креста Господня. Прихожане молитвенно почтили спасительные Страдания Господа Иисуса Христа и обретение Животворящего Креста, многие приобщились Святых Христовых Тайн.<br><br>

    Семнадцать столетий сохраняется в мире этот дивный праздник, вобравший в себя три разновременных события истории Животворящего Древа. Это обретение Крестного Древа, и построение храма Воскресения Господня на Голгофе, и внесение Животворящего Креста в этот храм, и возвращение Животворящего Древа из персидского плена и водворение его на Голгофе, на своем первоначальном месте.<br><br>

    «Церковь всем явно провозглашает, что «крест есть хранитель всея вселенныя, крест — красота Церкви, крест — царей держава, крест Ангелов слава и демонов язва».<br><span class="text-right">Прп. Амвросий Оптинский</span></p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/181.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/181.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/182.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/182.jpg'>
            </a>
        </div>
    </div>



</div>