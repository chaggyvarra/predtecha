<h2>День памяти святителя Николая Чудотворца.</h2>
<p>19 декабря 2019 года, в день памяти святителя Николая Чудотворца, в Иоанно — Предтеченском храме была совершена праздничная Божественная Литургия. После окончания Литургии состоялся крестный ход в честь памяти Святителя Николая.
<br><br>
    Святитель Николай Чудотворец был подобен другим ревнителям славы Божией пророку Илии и Крестителю Господню Иоану. Вся его жизнь представляет непрерывный ряд благодеяний, оказанных страждущим людям и сейчас он в темницах заключенных посещает, и в море погибающих спасает.
    <br><br>
    Помолимся ему, чтобы он испросил нам дары духовные: смирение, кротость и любовь. Святителю отче Николае, моли Христа Бога спастися душам нашим! Аминь.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/194.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/194.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/195.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/195.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/196.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/196.jpg'>
            </a>
        </div>
    </div>


</div>