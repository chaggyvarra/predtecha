<h2>День православной молодёжи.</h2>
<p><img src="/uploads/den_molodeji_1.jpg" align="right" width="250" class="g-pl-5">15 февраля отмечается Всемирный день православной молодёжи. В Жуковском благочинии состоялся целый ряд мероприятий, посвящённых этому празднику.</p>

<p>Так, 14 февраля настоятель Михаило-Архангельского храма протоиерей Алексий Агапов встретился с учащимися 6, 7 и 9 классов ОАНО «Свободная школа» г. Жуковского. Отец Алексий рассказал ребятам о празднике Сретения Господня, о духовном поиске и вечных ценностях. В ходе беседы ребята смогли обсудить актуальные темы духовной жизни современной молодёжи.</p>

<p>15 февраля настоятель Иоанно-Предтеченского храма священник Сергий Симаков провёл беседу о Празднике Сретения в 9-м классе школы № 15 с русским этнокультурным компонентом, лейтмотивом которой стала тема личной встречи с Богом для каждого человека.</p>

<p><img src="/uploads/den_molodeji_2.jpg" align="left" width="250" class="g-pr-5">17 февраля на приходе Иоанно-Предтеченского храма ученики НОУ «Православная школа "Образ"» посёлка Малаховка проводили научные изыскания по предмету «Духовное краеведение», в рамках которых ребята и родители встретились со  священником Сергием Симаковым.</p>

<p>День православной молодёжиОтец Сергий рассказал гостям о празднике Сретения Господня, об истории разрушенного храма и его возрождении в конце XX в., о новомучениках Жуковских, в том числе о мученике  Димитрии Ильинском, память которого совершалась в тот день. Затем ответил на вопросы. Был отснят видеофильм о жизни прихода. В заключение отец Сергий подарил ребятам книги митрополита Тихона (Шевкунова) «Несвятые святые».</p>

<p>17 февраля участники Православного молодёжного клуба «Встреча» собрались на традиционную беседу о Сретении Господнем, по окончании которой состоялся просмотр фотоотчёта о Православном молодёжном фестивале «Братья», прошедшем 3-10 февраля в Рязани. Духовник клуба священник Сергий Симаков напомнил участникам о том, что 17 февраля совершается память новомученика Жуковского Димитрия Ильинского и рассказал о посещении его младшей дочери Фаины Дмитриевны Соболевой.</p>