<h2>Новомученики Жуковские</h2>
<p>21 октября в Иоанно – Предтеченском храме состоялась праздничная литургия, посвященная собору новомучеников Жуковских, принявших мученическую кончину за Христа и Церковь Православную, священномученика Петра Озерецковского, мученика Дмитрия Ильинского и мученицу Ольгу Евдокимову, подвергшихся гонениям в годы Октябрьской революции 1917 года.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/187.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/187.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/188.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/188.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/189.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/189.jpg'>
            </a>
        </div>
    </div>

</div>