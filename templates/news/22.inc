<h2>Успение Пресвятой Богородицы.</h2>
<p>28 августа весь христианский мир торжественно и радостно празднует день светлого преставления Пресвятой Владычицы нашей Богородицы и Приснодевы Марии. Слово «успение» означает сон, казалось бы, встреча со смертью печальное, исполненное слез событие, но святая Церковь облекается в одежды торжества, радуется и ликует, и нас к тому призывают, потому что после успения Божией Матери последовало радостное пробуждение. Сам Господь Славы с ангелами и святыми явился для встречи Своей матери, в присутствии всех апостолов, собравшихся в этот день в Иерусалиме.</p>
<p>Успение Пресвятой Богородицы является завершающим праздником в церковном году. В этот светлый и торжественный день в Иоанно – Предтеченском храме была совершена праздничная Божественная Литургия, где прихожане и паломники причастились Святых Христовых Тайн.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/174.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/174.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/175.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/175.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/176.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/176.jpg'>
            </a>
        </div>
    </div>


</div>