<h2>Вербное воскресенье</h2>
<p><img src="/uploads/verba_0.jpg" align="right" width="250" class="g-pl-5">По церковной традиции, ровно за неделю до Пасхи, празднуется Вербное воскресенье, этот праздник так же имеет второе название – Вход Господень в Иерусалим.<br><br>

    Верующие в этот день вспоминают, как за неделю до своего распятия Христос пришел в столицу Израиля. Жители города радостно и торжественно встречали его, как обещанного им мессию, Посланника Божия, стелили под ноги его пальмовые ветви и одежду. Пальмовая ветвь на Ближнем востоке издревле символизировала жизнь, победу, праведность, чистоту. В Римской империи, процессии с пальмовыми ветвями приветствовали царей и полководцев-победителей. Спаситель видел искреннюю радость встречающих его людей, но знал, как скоро все изменится и восторженные приветствия, сменятся на всеобщий гул – «распни его!»<br><br>

    Именно поэтому праздник Входа Господня в Иерусалим всегда был в церковной традиции одновременно и грустным, и радостным, это начало Его пути на Голгофу. И в тоже время за будущей позорной смертью последует Воскресение, которое откроет и другим людям путь к вечной жизни.<br><br>

    Во время всенощного бдения на кануне праздника и после утренней Божественной литургии были освящены заготовленные за ранее ветви вербы, которые раздавались пришедшим на богослужение людям.<br><br>

    Во время богослужения в день праздника Входа Господня в Иерусалим мы мысленно возвращаемся в Евангельские времена и незримо встречаем грядущего в Иерусалим Спасителя. Он идет отдать свою жизнь за людей и тем самым – открыть двери в Царство Небесное.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/81.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/81.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/82.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/82.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/83.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/83.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/84.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/84.jpg'>
            </a>
        </div>
    </div>
</div>