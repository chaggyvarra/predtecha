<h2>Благовещение</h2>
<p><img src="/uploads/blagoveschenie.jpg" align="right" width="250" class="g-pl-5">07 апреля 2019 года, в четвертую неделю Великого поста, в Иоанно – Предтеченском храме встретили Праздник Благовещения Пресвятой Богородицы.</p>

<p> В этот день была совершена Божественная литургия, большое количество прихожан пришли помолиться, прославить Божию Матерь и приобщиться Святых Христовых Тайн.</p>
<p>
    По традиции по окончании богослужения настоятель священник Сергий Симаков  вместе с юными прихожанами выпустил белых голубей.</p>

<p>  Слово святого праведного Иоанна Кронштадтского на Праздник Благовещения Пресвятой Богородицы, раскрывает нам сущность и смысл происшедшего события, таинства воплощения и вочеловечения Сына Божия.</p>

<p>  «Таинство, свершившееся в нынешний день, приводит в изумление не только человеческие, но и все ангельские, высокие умы. Недоумевают и они, как Бог безначальный, необъятный, неприступный, нисшёл до образа раба и стал человеком, не перестав быть Богом и нимало не умалив славы Божественной? Как Дева могла вместить в пречистой утробе нестерпимый огонь Божества, и остаться неповреждённою, и пребыть на веки Матерью Бога воплощённого? Так велико, чудно, такой Божественной премудрости исполнено это таинство благовещения Архангелом Пресвятой Деве воплощения Сына Божия от Неё! Радуйтесь, земнородные, радуйтесь, особенно верные души христианские, но радуйтесь с трепетом перед величием таинства, как обложенные скверной греха; радуйтесь, но немедленно искренним и живым, глубоким покаянием очищайте себя благодатью Божьей от скверны греха. Возвеличьте чистыми сердцами и устами Матерь Божью, возвеличенную и превознесённую над всеми тварями, Ангелами и человеками, возвеличенную Самим Богом, Творцом всякой твари, и помните, что таинство воплощения и вочеловечения Сына Божия совершилось для нашего спасения от греха, проклятья, праведно на нас изречённого в начале от Бога за грехи, и от смерти временной и вечной. Со страхом и радостью примите Господа, грядущего к нам водворить на земле, в сердцах и душах наших царство небесное, царство правды, мира и радости в Духе Святом, и возненавидьте богоненавистный грех, злобу, нечистоту, невоздержание, гордость, жестокосердие, немилосердие, себялюбие, плотоугодие, всякую неправду. Христос для того на землю сошёл, чтобы нас на небеса возвести».</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/77.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/77.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/78.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/78.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/79.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/79.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/80.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/80.jpg'>
            </a>
        </div>
    </div>
</div>