<h2>Праздник Крещения Господня.</h2>

<p>Праздник Крещения Господня или другое название Богоявление это один из двунадесятых праздников. Двунадесятые праздники тесно связаны с событиями земной жизни Господа Иисуса Христа и Богоматери. В этот день 19 января православные христиане вспоминают евангельское событие — как пророк Иоанн-Предтеча крестил Господа Иисуса Христа в реке Иордан.<br><br>

    Все знают, что в этот день и накануне, в сочельник, бывает освящение воды. Обычно в эти дни в храм приходят даже те, кто обычно не посещает службы — «за водой».<br><br>

    В Иоанно — Предтеченском храме празднование началось с Навечерия Богоявления, затем было совершено Всенощное бдение и Божественная литургия с чином Великого освящения воды, затем каждый желающий смог набрать себе домой святой воды.
    <br><br>
    На территории храма у реки Москвы, было организованно место - Иордань для погружения после обряда освящения открытых вод, где все желающие смогли окунуться в холодной январской воде. После омовения всем присутствующим был предложен чай с печеньем.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/213.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/213.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/214.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/214.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/215.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/215.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/216.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/216.jpg'>
            </a>
        </div>
    </div>

    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/217.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/217.jpg'>
            </a>
        </div>
    </div>



</div>