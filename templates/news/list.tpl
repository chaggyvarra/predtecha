<div class="container g-mt-100">
    {foreach $news as $news_item}
        <!-- item starts -->
        <div class="row g-mb-30">
            <!-- Article Image -->
            <div class="col-md-3 g-width-200 g-height-150 g-overflow-hidden">

                <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="{$news_item.cover}">

            </div>
            <!-- End Article Image -->

            <!-- Article Content -->
            <div class="col-md-9 align-self-center">
                <!-- Article Info -->
                <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                    <li class="list-inline-item">
                        Опубликовано: {$news_item.date}
                    </li>
                    <li class="list-inline-item">/</li>
                    <li class="list-inline-item">
                        Автор: Администратор
                    </li>
                </ul>
                <!-- End Article Info -->

                <!-- Article Title -->
                <h3 class="h3 g-font-weight-300 g-mb-15">
                    <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id={$news_item.id}">{$news_item.title}</a>
                </h3>
                <!-- End Article Title -->

                <!-- Article Text -->
                <div class="g-mb-30">
                    <p>{$news_item.anounce}</p>
                </div>
                <!-- End Article Text -->
            </div>
            <!-- End Article Content -->
        </div>
        <!-- item ends -->

    {/foreach}

    {$pagination}
</div>