<h2>Ежегодный велопробег</h2>
<p><img src="/uploads/news2405.jpg" align="right" width="250" class="g-pl-5">18 мая в г. Жуковском прошёл ежегодный городской велопробег «За трезвый образ жизни!», организованный Жуковским благочинием при поддержке администрации г.о. Жуковский. Перед участниками велопробега, количество которых превысило 260 человек, обратился с напутственным словом клирик Преображенского храма протоиерей Александр Лыков, который и сам стал непосредственным участником велопробега.<br><br>

    С территории Пантелеймоновского и Преображенского храмов, под звон колоколов колонна проследовала до городской площади, где участников приветствовали глава г.о. Жуковский Ю. В. Прохоров и протоиерей Александр Лыков. После приветственного напутствия о здоровом и трезвом образе жизни и личном спасении, колонна направилась на приход храма Рождества Иоанна Предтечи, где был отслужен праздничный молебен с водосвятием.<br><br>

    После молебна и пастырской проповеди участники были приглашены на трапезу, затем состоялись розыгрыши призов, конкурсы, турниры по армрестлингу мужские и женские, подвижные игры, а так же экскурсия по территории прихода, посещение историко-археологического кабинета с просмотром фильма о новомученниках Жуковских и истории храма Рождества Иоанна Предтечи.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/117.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/117.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/118.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/118.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/119.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/119.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/120.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/120.jpg'>
            </a>
        </div>
    </div>

</div>