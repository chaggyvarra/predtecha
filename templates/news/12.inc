<h2>Поминовение воинов</h2>
<p><img src="/uploads/news1005.jpg.jpg" align="right" width="250" class="g-pl-5">С 1994 года 9 мая в День Победы во всех православных храмах России совершается поминовение воинов, «за Веру, Отечество и народ жизнь свою положивших, и всех мученически погибших в годы Великой Отечественной войны». Наша земля славится многими героями, отдавшими свою жизнь на поле брани во благо своего народа. Воины, погибшие при защите своей Родины и отечества, особо почитаются нашей церковью, как говорится в Евангелии, «нет большей той любви, как если кто положит душу свою за други своя». На протяжении многих веков вера православная укрепляла боевой дух воинов Вооруженных сил, помогала преодолевать трудности и лишения воинской службы, звала на подвиги во имя родной земли.
<br><br>
    Православная церковь всегда разделяла скорби и беды народные, оказывая духовную и материальную поддержку. В день начала войны 22 июня 1941 года первым обратился к согражданам глава Русской Православной Церкви митрополит Сергий, благословив на защиту Отечества и принял участие в сборе средств на оборону, именно на средства собранные прихожанами в православных храмах, была сформирована танковая колонна имени Дмитрия Донского. Не оставалось в стороне и духовенство, разделившее с народом все тяготы войны.
    <br><br>
    В этот весенний и солнечный день, в Иоанно – Предтеченском храме г. Жуковского был отслужен благодарственный молебен Господу Богу за дарование Победы в Великой Отечественной войне 1941–1945 годов. Также по благословению Святейшего Патриарха Московского и всея Руси Кирилла, был совершен заупокойный колокольный звон в память о погибших в годы Великой Отечественной войны, эта акция прошла во всех храмах Русской Православной Церкви.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/107.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/107.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/108.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/108.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/109.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/109.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/110.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/110.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/111.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/111.jpg'>
            </a>
        </div>
    </div>

</div>