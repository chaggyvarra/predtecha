<div class="container g-mt-100">
<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news190120.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 19.01.2020
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=35">Праздник Крещения Господня.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p> В Иоанно — Предтеченском храме празднование началось с Навечерия Богоявления, затем было совершено Всенощное бдение и Божественная литургия с чином Великого освящения воды</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news120120.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 12.01.2020
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=34">Рождественский праздник.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p> 12 января 2020 года в Иоанно — Предтеченском Храме, прошел Рождественский праздник для детей и взрослых.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news070120.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 07.01.2020
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=33">Рождество Христово.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p> «И Слово стало плотию!.. Для того, чтобы нас земных сделать небесными, грешных — святыми...»</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2112.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 21.12.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=32">День памяти святителя Николая Чудотворца.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>19 декабря 2019 года, в день памяти святителя Николая Чудотворца, в Иоанно — Предтеченском храме была совершена праздничная Божественная Литургия.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0612.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 06.12.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=31">Праздник введения во храм Пресвятой Богородицы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>4 декабря Православная церковь празднует Введение во храм Пресвятой Богородицы, один из 12 главных (двунадесятых) праздников.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0811.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 08.11.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=30">Паломническая поездка</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>7 ноября прихожане Иоанно—Предтеченского храма вместе с настоятелем отцом Сергием совершили паломническую поездку в Троице - Сергиеву Лавру.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0511.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 05.11.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=29">Празднование Казанской иконы Божией Матери</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>4 ноября церковь празднует память Казанской иконы Божией Матери.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2210.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 22.10.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=28">Новомученики Жуковские</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>21 октября в Иоанно – Предтеченском храме состоялась праздничная литургия, посвященная собору новомучеников Жуковских.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news1410.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 14.10.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=27">Покров Пресвятой Богородицы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>14 октября в праздник Покрова Пресвятой Богородицы, православные верующие вспоминают чудо явления Богородицы, святым Андрею и Епифанию, во время Богослужения в Константинополе.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news1010.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 10.10.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=26">Сергий Радонежский</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>Совершением Божественной Литургии память Святого преподобного Сергия Радонежского, встретили прихожане Иоанно-Предтеченского храма.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news3009.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 30.09.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=25">Крестовоздвижение</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>Семнадцать столетий сохраняется в мире этот дивный праздник, вобравший в себя три разновременных события истории Животворящего Древа.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2509.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 25.09.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=24">Пастырские беседы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>В трапезной в теплой домашней атмосфере собравшиеся могли задать настоятелю волнующие их вопросы.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2409.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 24.09.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=23">Рождество Пресвятой Богородицы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>БОЖИЯ МАТЕРЬ — УТРЕННЯЯ ЗВЕЗДА, ПРЕДВОЗВЕСТИВШАЯ ЯВЛЕНИЕ СОЛНЦА ПРАВДЫ, ХРИСТА СПАСИТЕЛЯ</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2908.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 29.08.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=22">Успение Пресвятой Богородицы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>28 августа весь христианский мир торжественно и радостно празднует день светлого преставления Пресвятой Владычицы нашей Богородицы и Приснодевы Марии.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2008.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 20.08.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=21">Яблочный Спас</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>Одно из самых значимых и ярких событий из жизни Спасителя является Преображение Господа нашего Иисуса Христа, которое произошло незадолго до Его страдания.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news1507.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 15.07.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=20">Бывалинский фестиваль</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>Уже в четырнадцатый раз в деревне Бывалино на территории храма Святого Великомученика Никиты прошел Международный Фестиваль Кузнечного Искусства.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0807.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 08.07.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=19">Престольный праздник</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>7 июля 2019 года в день памяти Рождества Иоанна Предтечи, в приходе Иоанно – Предтеченского храма встретили престольный праздник.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2706.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 27.06.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=18">Военно-патриотическое мероприятие «Чтобы помнили…» в Жуковском</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>21 и 22 июня в канун 78-й годовщины начала Великой Отечественной войны на приходе Иоанно-Предтеченского храма г.Жуковского прошло военно-патриотическое мероприятие «Чтобы помнили…».</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2006.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 20.06.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=17">Праздник Святой Троицы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>16 июня в Иоанно – Предтеченском храме, в Праздник Святой Троицы, была совершена праздничная Божественная литургия.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0906.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 09.06.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=16">Рисуем храм Иоанна Предтечи</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>8 июня на территории Иоанно – Предтеченского храма, состоялось арт событие «Рисуем храм Иоанна Предтечи». В мероприятии приняли участие дети и взрослые.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news3005.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 30.05.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=15">Литургия в Иоанно-Предтеченском храме</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>26 мая 2019 г. в 5-ю неделю по Пасхе на Божественной литургии в Иоанно-Предтеченском храме в качестве певчих выступала капелла им. прп. Сергия Радонежского учеников МОУ №15.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2405.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 24.05.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=14">Ежегодный велопробег</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>18 мая в г. Жуковском прошёл ежегодный городской велопробег «За трезвый образ жизни!», организованный Жуковским благочинием.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news2305.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 23.05.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=13">Празднование перенесения мощей св. Николая</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>22 мая 2019 года, в день празднования перенесения мощей святителя Николая, в Иоанно – Предтеченском Храме г. Жуковского была совершена Божественная литургия.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news1005.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 10.05.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=12">Поминовение воинов</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>С 1994 года 9 мая в День Победы во всех православных храмах России совершается поминовение воинов, «за Веру, Отечество и народ жизнь свою положивших.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/news0505.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 5.05.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=11">Cовершили Божественную Литургию и крестный ход</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>В Субботу Светлой Седмицы, в Иоанно – Предтеченском храме, совершили Божественную Литургию и крестный ход.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/greatsuturday.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 27.04.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=10">Богослужение в Великую субботу</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>В Великую субботу, в Иоанно – Предтеченском храме были совершены часы, изобразительны и Божественная Литургия святителя Василия Великого.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/plashinica.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 26.04.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=9">Богослужение Выноса Плащаницы</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>В Страстную Пятницу, в Иоанно - Предтеченском храме на Вечере Великой субботы, настоятель храма отец Сергий совершил богослужение Выноса Плащаницы.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/verba_0.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 22.04.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=7">Вербное воскресенье.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>По церковной традиции, ровно за неделю до Пасхи, празднуется Вербное воскресенье, этот праздник так же имеет второе название – Вход Господень в Иерусалим.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/subbotnik.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 21.04.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=8">Субботник.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>По доброй традиции в конце Великого поста перед началом страстной седмице в Лазареву субботу прихожане провели субботник.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->


<!-- item starts -->
<div class="row g-mb-30">
    <!-- Article Image -->
    <div class="col-md-3">
        <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/blagoveschenie.jpg">
    </div>
    <!-- End Article Image -->

    <!-- Article Content -->
    <div class="col-md-9 align-self-center">
        <!-- Article Info -->
        <ul class="list-inline font-italic g-font-size-12 g-mb-10">
            <li class="list-inline-item">
                Опубликовано: 8.04.2019
            </li>
            <li class="list-inline-item">/</li>
            <li class="list-inline-item">
                Автор: Администратор
            </li>
        </ul>
        <!-- End Article Info -->

        <!-- Article Title -->
        <h3 class="h3 g-font-weight-300 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=6">Благовещение.</a>
        </h3>
        <!-- End Article Title -->

        <!-- Article Text -->
        <div class="g-mb-30">
            <p>07 апреля 2019 года, в четвертую неделю Великого поста, в Иоанно – Предтеченском храме встретили Праздник Благовещения Пресвятой Богородицы.</p>
        </div>
        <!-- End Article Text -->
    </div>
    <!-- End Article Content -->
</div>
<!-- item ends -->

    <!-- item starts -->
    <div class="row g-mb-30">
        <!-- Article Image -->
        <div class="col-md-3">
            <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/den_molodeji_1.jpg">
        </div>
        <!-- End Article Image -->

        <!-- Article Content -->
        <div class="col-md-9 align-self-center">
            <!-- Article Info -->
            <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                <li class="list-inline-item">
                    Опубликовано: 18.02.2019
                </li>
                <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                    Автор: Администратор
                </li>
            </ul>
            <!-- End Article Info -->

            <!-- Article Title -->
            <h3 class="h3 g-font-weight-300 g-mb-15">
                <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=4">День православной молодёжи.</a>
            </h3>
            <!-- End Article Title -->

            <!-- Article Text -->
            <div class="g-mb-30">
                <p>15 февраля отмечается Всемирный день православной молодёжи. В Жуковском благочинии состоялся целый ряд мероприятий, посвящённых этому празднику.</p>
            </div>
            <!-- End Article Text -->
        </div>
        <!-- End Article Content -->
    </div>
    <!-- item ends -->

    <!-- item starts -->
    <div class="row g-mb-30">
        <!-- Article Image -->
        <div class="col-md-3">
            <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/prestolniy_prazdnik.jpg">
        </div>
        <!-- End Article Image -->

        <!-- Article Content -->
        <div class="col-md-9 align-self-center">
            <!-- Article Info -->
            <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                <li class="list-inline-item">
                    Опубликовано: 12.02.2019
                </li>
                <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                    Автор: Администратор
                </li>
            </ul>
            <!-- End Article Info -->

            <!-- Article Title -->
            <h3 class="h3 g-font-weight-300 g-mb-15">
                <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=5">Престольный праздник.</a>
            </h3>
            <!-- End Article Title -->

            <!-- Article Text -->
            <div class="g-mb-30">
                <p>10 февраля, в день памяти новомучеников и исповедников Церкви Русской, в Преображенском храме г. Жуковского состоялся престольный праздник.</p>
            </div>
            <!-- End Article Text -->
        </div>
        <!-- End Article Content -->
    </div>
    <!-- item ends -->


    <!-- item starts -->
    <div class="row g-mb-30">
        <!-- Article Image -->
        <div class="col-md-3">
            <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/uploads/846s.jpg">
        </div>
        <!-- End Article Image -->

        <!-- Article Content -->
        <div class="col-md-9 align-self-center">
            <!-- Article Info -->
            <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                <li class="list-inline-item">
                    Опубликовано: 09.01.2019
                </li>
                <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                    Автор: Администратор
                </li>
            </ul>
            <!-- End Article Info -->

            <!-- Article Title -->
            <h3 class="h3 g-font-weight-300 g-mb-15">
                <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=1">Просим всех принять жертвенное участие в благом деле восстановления порушенных православных святынь!</a>
            </h3>
            <!-- End Article Title -->

            <!-- Article Text -->
            <div class="g-mb-30">
                <p>Благотворительный Фонд Московской епархии по восстановлению порушенных святынь просит всех принять жертвенное участие в благом деле восстановления порушенных православных святынь!</p>
            </div>
            <!-- End Article Text -->
        </div>
        <!-- End Article Content -->
    </div>
    <!-- item ends -->

    <!-- item starts -->
    <div class="row g-mb-30">
        <!-- Article Image -->
        <div class="col-md-3">
            <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/assets/pic/news/2_1.jpg">
        </div>
        <!-- End Article Image -->

        <!-- Article Content -->
        <div class="col-md-9 align-self-center">
            <!-- Article Info -->
            <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                <li class="list-inline-item">
                    Опубликовано: 07.01.2019
                </li>
                <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                    Автор: Администратор
                </li>
            </ul>
            <!-- End Article Info -->

            <!-- Article Title -->
            <h3 class="h3 g-font-weight-300 g-mb-15">
                <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=2">Мастерской народного творчества при Пантелеимоновском приходе исполняется 25 лет.</a>
            </h3>
            <!-- End Article Title -->

            <!-- Article Text -->
            <div class="g-mb-30">
                <p>На приходе Пантелеимоновского храма, в здании Воскресной школы проходит выставка работ мастеров рукоделия, ручного узорного ткачества, росписи по дереву и других традиционных ремёсел. Выставка посвящена памяти прихожанки Пантелеимоновского храма, основательнице мастерской Ларисе Александровне Кожевниковой (26.09.1923—27.12.1997) </p>
            </div>
            <!-- End Article Text -->
        </div>
        <!-- End Article Content -->
    </div>
    <!-- item ends -->

    <!-- item starts -->
    <div class="row g-mb-30">
        <!-- Article Image -->
        <div class="col-md-3">
            <img class="img-fluid g-mb-20 g-mb-0--md g-width-200" src="/assets/pic/news/3_1.jpg">
        </div>
        <!-- End Article Image -->

        <!-- Article Content -->
        <div class="col-md-9 align-self-center">
            <!-- Article Info -->
            <ul class="list-inline font-italic g-font-size-12 g-mb-10">
                <li class="list-inline-item">
                    Опубликовано: 07.01.2019
                </li>
                <li class="list-inline-item">/</li>
                <li class="list-inline-item">
                    Автор: Администратор
                </li>
            </ul>
            <!-- End Article Info -->

            <!-- Article Title -->
            <h3 class="h3 g-font-weight-300 g-mb-15">
                <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/?id=3">Рождественский праздник для детей.</a>
            </h3>
            <!-- End Article Title -->

            <!-- Article Text -->
            <div class="g-mb-30">
                <p>13 января в 11:00 на приходе Пантелеимоновского храма состоится Рождественский праздник детской Воскресной школы.</p>
            </div>
            <!-- End Article Text -->
        </div>
        <!-- End Article Content -->
    </div>
    <!-- item ends -->

    <?php //echo getPagination(3, 18, $pages[0]); ?>

</div>