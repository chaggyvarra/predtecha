<h2>Cовершили Божественную Литургию и крестный ход</h2>
<p><img src="/uploads/news0505.jpg.jpg" align="right" width="250" class="g-pl-5">4 мая 2019 года, в Субботу Светлой Седмицы, в Иоанно – Предтеченском храме, совершили Божественную Литургию и крестный ход. Во время крестного хода, прихожане вместе с настоятелем храма отцом Сергием, в пасхальных песнопениях прославили Воскресенье нашего Спасителя Иисуса Христа.
<br><br>
    В этот Светлый день, побывать на богослужении и причаститься приехала, младшая дочь новомученника Дмитрия Ильинского Фаина Дмитриевна Соболева – Ильинская.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/103.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/103.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/104.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/104.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/105.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/105.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/106.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/106.jpg'>
            </a>
        </div>
    </div>

</div>