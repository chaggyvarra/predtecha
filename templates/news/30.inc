<h2>Паломническая поездка в Троице-Сергиеву Лавру.</h2>
<p>7 ноября прихожане Иоанно—Предтеченского храма вместе с настоятелем священником Сергием совершили паломническую поездку в Троице - Сергиеву Лавру. Все участники поездки помолились во время утренних молитв и полунощницы в Троицком соборе у мощей преподобного Сергия Радонежского, участвовали в  Божественной литургии, те, кто готовился приобщились Святых Христовых Тайн, а после литургии все приняли участие в молебне с пением акафиста Преподобному. Затем паломники посетили Никоновский придел и Серапионову палату, где поклонились святыням Лавры.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/193.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/193.jpg'>
            </a>
        </div>
    </div>


</div>