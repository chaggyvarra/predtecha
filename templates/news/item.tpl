<div class="container g-mt-130">
    <section class="g-brd-top g-brd-bottom g-brd-gray-light-v4 g-py-10">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main" href="/">Главная</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main" href="/news/">Новости</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span class="clip">{$news_item.title}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <h2>{$news_item.title}</h2>
    <div class="text-right g-font-size-11 g-color-gray-light-v1">{$news_item.date}</div>
    <p>{$news_item.text}</p>

    {if count($news_item.pics) gt 0}
    <div class="row">
        {foreach $news_item.pics as $news_pic}

            <div class='col-md-2 g-mb-30--md'>
                <div class='g-brd-around g-brd-gray-light-v4--hover'>
                    <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/{$news_pic}' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                        <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/{$news_pic}'>
                    </a>
                </div>
            </div>
        {/foreach}

    </div>
    {/if}
</div>