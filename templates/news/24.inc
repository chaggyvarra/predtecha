<h2>Пастырские беседы.</h2>
<p>22 сентября в день Попразднства Рождества Пресвятой Богородицы после Божественной Литургии, по просьбе прихожан состоялась первая беседа по вопросам веры с настоятелем Иоанно – Предтеченского храма иереем Сергием Симаковым. Беседа была посвящена первой части Литургии: Проскомидии. Отец Сергий подробно рассказал, как совершается приготовление вещества (хлеба и вина) для совершения таинства Евхаристии. Так как Проскомидия совершается священником на жертвеннике, при закрытом алтаре, вполголоса, у прихожан не всегда складывается истинное понимание совершаемых действий. В трапезной в теплой домашней атмосфере собравшиеся могли задать настоятелю волнующие их вопросы. Возможно, такие беседы перерастут в духовно-просветительский проект «Пастырские беседы по вопросам веры», что поможет прихожанам придти к подлинной общинной жизни в полноте истины Христовой и расширить христианское мировоззрение.</p>

<div class="row">
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/178.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/178.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/179.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/179.jpg'>
            </a>
        </div>
    </div>
    <div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/180.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/180.jpg'>
            </a>
        </div>
    </div>


</div>