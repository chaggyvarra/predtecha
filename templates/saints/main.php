<ul class="list-unstyled">
    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="baptist">Иоанн Предтеча</a></h5>
        </div>
        <p>О пророке Иоанне Крестителе Господь Иисус Христос сказал: «Из рожденных женами не восставал (пророк) больший Иоанна Крестителя» (Мф.11:11). Иоанн Креститель прославляется Церковью, как «ангел, и апостол, и мученик, и пророк, и свечник, и друг Христов, и пророков печать, и ходатай ветхой и новой благодати, и в рожденных пречестнейший, и светлый Слова глас».</p>
    </li>
    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="martyrs">Новомученники Жуковские</a></h5>
        </div>
        <div class="d-flex justify-content-between">
            <ul class="u-list-inline">
                <li class="list-inline-item mr-5"><a href="martyrs#petr">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/martyrs/thumb/2.jpg">
                    <p>ОЗЕРЕЦКОВСКИЙ <br>Петр Александрович</p>
                    </a></li>
                <li class="list-inline-item mr-5"><a href="martyrs#dmitry">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/martyrs/thumb/1.jpg">
                    <p>ИЛЬИНСКИЙ <br>Димитрий Иванович</p>
                </a></li>
                <li class="list-inline-item mr-0"><a href="martyrs#olga">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/martyrs/thumb/3.jpg">
                    <p>ЕВДОКИМОВА <br>Ольга Васильевна</p>
                </a></li>
            </ul>
        </div>
    </li>

    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="paraskeva">Параскева Пятница Покровительница</a></h5>
        </div>
        <p>Святая мученица Параскева, нареченная Пятницей, жила в III веке в Иконии в богатой и благочестивой семье. «Параскева» в переводе с греческого и означает «Пятница» – родители святой дали ей это имя в силу особого почитания дня страданий Господних.</p>
    </li>

    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="st_nicholas">Покровитель Св. Николай</a></h5>
        </div>
        <p>Святитель Николай Чудотворец, архиепископ Мир Ликийских, прославился как великий угодник Божий. Имя великого угодника Божия, святителя и чудотворца Николая, скорого помощника и молитвенника за всех, притекающих к нему, прославилось во всех концах земли, во многих странах и народах.</p>
    </li>
</ul>