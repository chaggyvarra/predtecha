<div class="container g-mt-100">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-unstyled">
                <li><a href="baptist" class="g-color-gray-dark-v1 g-font-size-18--md">Иоанн Предтеча</a></li>
                <li><a href="martyrs" class="g-color-gray-dark-v1 g-font-size-18--md">Новомученники Жуковские</a></li>
                <li><a href="paraskeva" class="g-color-gray-dark-v1 g-font-size-18--md">Параскева Пятница</a></li>
                <li><a href="st_nicholas" class="g-color-gray-dark-v1 g-font-size-18--md">Святитель Николай</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <?php include($rightColumnPage);?>
        </div>
    </div>
</div>