<div class="row">
    <div class="col-lg-6">
        <!-- Article -->
        <div class="row g-mx-5--sm g-mb-30">
            <!-- Article Image -->
            <div class="col-sm-5 g-px-0--sm">
                <div class="g-height-200 g-bg-size-cover" style="background-image: url(../../assets/img-temp/500x450/img5.jpg);"></div>
            </div>
            <!-- End Article Image -->

            <!-- Article Content -->
            <div class="col-sm-7 g-px-0--sm">
                <div class="u-info-v1-1 g-height-200 g-bg-gray-light-v5 g-bg-gray-light-gradient-v1--after g-pa-20-30-0">
                    <h3 class="h6 g-font-weight-600 text-uppercase">
                        <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">Mauris tellus magna, pretium</a>
                    </h3>
                    <p class="small g-color-primary">
                        <time>April 27, 2016</time>
                    </p>

                    <p>Pulvinar rutrum libero, id lobortis augue temporin gravida orci maximus. In feugiat ipsum a quam lacinia eleifend sem dapibus a. Mauris pulvinar rutrum libero, in gravida orci maximus feugiat ipsum a quam gravida orci.</p>
                </div>
            </div>
            <!-- End Article Content -->
        </div>
        <!-- End Article -->
    </div>

    <div class="col-lg-6">
        <!-- Article -->
        <div class="row g-mx-5--sm g-mb-30">
            <!-- Article Image -->
            <div class="col-sm-5 g-px-0--sm">
                <div class="g-height-200 g-bg-size-cover" style="background-image: url(../../assets/img-temp/500x450/img2.jpg);"></div>
            </div>
            <!-- End Article Image -->

            <!-- Article Content -->
            <div class="col-sm-7 g-px-0--sm">
                <div class="u-info-v1-1 g-height-200 g-bg-gray-light-v5 g-bg-gray-light-gradient-v1--after g-pa-20-30-0">
                    <h3 class="h6 g-font-weight-600 text-uppercase">
                        <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">Cras volutpat sed leout</a>
                    </h3>
                    <p class="small g-color-primary">
                        <time>April 27, 2016</time>
                    </p>

                    <p>Morbi et convallis metus, in congue mi. Nam placerat augue nec justo feugiat ipsum a quam luctus, id lobortis augue tempor. In feugiat ipsum a quam lacinia eleifend sem dapibus.</p>
                </div>
            </div>
            <!-- End Article Content -->
        </div>
        <!-- End Article -->
    </div>
</div>