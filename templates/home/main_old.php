<!-- Promo Block -->
<section class="dzsparallaxer auto-init g-mb-100 height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall " data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
    <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center" data-src="/assets/pic/main_new.jpg" style="height: 101%; background-position: top;"></div>
    <div class="preloader-semicircles"></div> <div class="dzsprxseparator--bigcurvedline"></div>

    <div class="container g-py-200 ">
        <div class="row">
            <div class="col-md-6 offset-md-6 text-right g-bg-white-opacity-0_5 row">
                <div class="col-md-5 g-pt-10">
                    <img src="/assets/pic/home/main_icon.jpg" class="img-fluid">
                </div>
                <div class="col-md-7 g-pt-10">

                    <h4 class="g-color-black g-font-weight-300 g-font-size-26 g-line-height-1_2 mb-4">
                        Официальный сайт  <br>Иоанно-Предтеченского храма <br>г. Жуковский
                    </h4>
                    <p class="g-color-black">РУССКАЯ ПРАВОСЛАВНАЯ ЦЕРКОВЬ <br>МОСКОВСКИЙ ПАТРИАРХАТ МОСКОВСКАЯ ЕПАРХИЯ</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Promo Block -->

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h3>Православный календарь</h3>
            <div id="pravCalendar">
            <?php
            include("calendar.php");
            ?>
            </div>
            <div class="g-hidden-sm-down">
            <h3 class="g-mt-30 ">Фотогалерея</h3>
            <?php
            $v = "leftside";
            include("gallery.php");
            ?>

            <p class="small text-right g-mb-30"><a href="/gallery/">все фото</a></p>
             </div>

            <? echo $adsContent; ?>

            <!--<div class="g-mt-10">
                <img src="/uploads/anounce140620.jpg" class="img-fluid">
            </div>


            <div class="g-mt-10">
                <a href="https://xn--h1acbxfam.xn--80aswg/rm" target="_blank"><img src="/assets/pic/home/rus_mir.jpg" class="img-fluid"></a>
            </div>-->
        </div>
        <div class="col-md-8">
            <h3>Новости прихода</h3>
            <div class="row">


                <?php echo $newsContent; ?>


            </div>
            <p class="small text-right g-mb-15"><a href="/news/">все новости</a></p>

            <div class="alert-warning g-pa-10">
                <h3>Внимание!</h3>
                <p>С 29 мая 2020 года разрешено посещение гражданами территории храмов, храмов вне богослужебного времени, богослужений на открытом воздухе при соблюдении следующих условий:<br>
                    1. Ограничение участие в богослужении лиц старше 65 лет страдающих хроническими заболеваниями, в первую очередь заболеваниями органов дыхания, сердечно-сосудистой системой и сахарным диабетом.<br>
                    2. Обязательное ношение защитных масок<br>
                    3. Соблюдение мер социального дистанцирование (не менее 1.5 метра между людьми)</p>
            </div>

<!--            <h3>Службы сегодня</h3>-->
            <?php echo $serviceToday; ?>




            <h3>История храма</h3>
            <figure class="sign-right">
                <p><img src="/assets/pic/photogallery/0.jpg" width="250"></p>
                <figcaption>Каменный храм в честь Рождества Иоанна Предтечи, разрушенный в 40-е годы XX века.</figcaption>
            </figure>
            <p>Вблизи озера Глушица через Москву-реку пролегал древнейший путь, освоенный, судя по остаткам городищ, еще в I тысячелетии до н. э. После присоединения Коломны к Московскому княжеству он становится одним из важнейших в экономическом и стратегическом отношении. Этот путь стал называться Астраханским трактом. В 1581 г., после смерти сына, Иван Грозный повелел поставить здесь монастырь в честь Рождества Иоанна Предтечи и устроить переправу через реку. Монахами в новом монастыре стали бывшие опричники. В писчей книге Приказа большого двора за 1631-1633 гг. указано: «За озером Здегошем, на монастырской земле стоял храм Рождества Иоанна Предтечи, храм великомученицы Параскевы, нарицаемой Пятницы, да придел Николая Чудотворца, по прошествии времени монастырь от разорения запустел». Заброшенные монастырские земли были переданы Патриарху Филарету и вскоре перешли в Дворцовое ведомство. На месте монастыря возникло село Новорождествено. Ни монастырь, ни его старые храмы не были восстановлены. В селе заново отстроили деревянный храм в честь Рождества Иоанна Предтечи. В 1710 г. Петр I подарил село Новорождествено и Раменскую волость графу И.А.Мусину-Пушкину в награду за подавление восстания в Астрахани в 1722 г., пожаловав его в вечное владение. В 1730 г. помещик возвел двухэтажную каменную церковь в честь Рождества Иоанна Предтечи, нижний придел которой освятили во имя святителя Николая. Имелось богатейшее мраморное Распятие. Среди многих старинных икон особо почиталась древняя Новодворская икона Божией Матери, в серебряной ризе. Раньше она принадлежала старинному роду князей Хромых-Волконских-Орловых. Церковный хор славился в округе. В 1852 г. село стало самым крупным в имении князей Волконских. После революции 1917 г. село Новорождествено переименовали в Новое. В 1938 г. церковь закрыли, решив разобрать, под тем предлогом, что для строительства новой школы нужен кирпич.</p>
            <p class="small text-right"><a href="history/">читать дальше...</a></p>




            <blockquote class="lead u-blockquote-v1 rounded g-pl-60 g-pr-30 g-py-30 g-mb-40 small"><?php echo $feofan; ?></blockquote>
            <div class="media">
                <img class="d-flex align-self-center rounded-circle g-brd-around g-brd-3 g-brd-white g-width-50 mr-3" src="/assets/pic/st_feofan.jpg" alt="свт. Феофан Затворник">
                <div class="media-body align-self-center">
                    <h4 class="h6 g-font-weight-700 g-mb-0">"Мысли на каждый день года"</h4>
                    <em class="g-color-gray-dark-v4 g-font-style-normal">свт. Феофан Затворник</em>
                </div>
            </div>


        </div>
    </div>
</div>