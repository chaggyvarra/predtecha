<?php
$c = file_get_contents("http://www.holytrinityorthodox.com/ru/calendar/calendar.php?dt=1&header=1&lives=5&trp=1&scripture=1");

$c = strip_tags($c, "<p></p><br><b></b><a></a>");

$cc = mb_convert_encoding($c, "UTF-8", "CP1251");

$content = explode("Тропари", $cc);

$calendar = strip_tags($content[0], "<p></p><br><a></a>");

echo $calendar;

$tropary = $content[1];

?>

<script>
    // Берем нужный div и все ссылки в нем:
    var div = document.getElementById('pravCalendar'),
        links = div.getElementsByTagName('a');

    // Вот таким нехитрым способом останавливаем переход по ссылкам:
    for(var i = 0, l = links.length; i < l; i++)
        links[i].onclick = function(){
            return false;
        };

    // А теперь делаем, что нам надо. Например:
    div.onclick = function(event){

// Берем кроссбраузерно событие и элемент, на котором оно произошло:
        if(!event) event = window.event;
        var target = event.target || event.srcElement;

// Проверяем, по ссылки ли кликнули:
        if(target.tagName.search(/a/i) === -1) return;

// Ну и собственно, вот ради чего все это:)
        if( confirm('Вы действительно хотите перейти по ссылке: ' + target.href) )
            window.open(target.href,'_blank');



    };
</script>