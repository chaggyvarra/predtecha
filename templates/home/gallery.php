<?php
if ($v == "leftside")
    $col = "col-md-6";
else
    $col = "col-md-2";


?>
<div class="row">
    <div class="col-md-6 g-mb-30--md">
        <div class="g-brd-around g-brd-gray-light-v4--hover">
            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/1.jpg" alt="example-image">
            </a>
        </div>
    </div>

    <div class="col-md-6 g-mb-30--md">
        <div class="g-brd-around g-brd-gray-light-v4--hover">
            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/2.jpg" alt="example-image">
            </a>
        </div>
    </div>

    <div class="col-md-6 g-mb-30--md">
        <div class="g-brd-around g-brd-gray-light-v4--hover">
            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/3.jpg" alt="example-image">
            </a>
        </div>
    </div>

    <div class="col-md-6 g-mb-30--md">
        <div class="g-brd-around g-brd-gray-light-v4--hover">
            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/4.jpg" alt="example-image">
            </a>
        </div>
    </div>
</div>