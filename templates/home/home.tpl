{literal}
<!-- Promo Block -->
<section class="dzsparallaxer auto-init g-mb-100 height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall " data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
    <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center" data-src="/assets/pic/main_new.jpg" style="height: 101%; background-position: top;"></div>
    <div class="preloader-semicircles"></div> <div class="dzsprxseparator--bigcurvedline"></div>

    <div class="container g-py-200 ">
        <div class="row">
            <div class="col-md-6 offset-md-6 text-right g-bg-white-opacity-0_5 row">
                <div class="col-md-5 g-pt-10">
                    <img src="/assets/pic/home/main_icon.jpg" class="img-fluid">
                </div>
                <div class="col-md-7 g-pt-10">

                    <h4 class="g-color-black g-font-weight-300 g-font-size-26 g-line-height-1_2 mb-4">
                        Официальный сайт  <br>Иоанно-Предтеченского храма <br>г. Жуковский
                    </h4>
                    <p class="g-color-black">РУССКАЯ ПРАВОСЛАВНАЯ ЦЕРКОВЬ <br>МОСКОВСКИЙ ПАТРИАРХАТ КОЛОМЕНСКАЯ ЕПАРХИЯ</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Promo Block -->
    <script>
        // Берем нужный div и все ссылки в нем:
        var div = document.getElementById('pravCalendar'),
                links = div.getElementsByTagName('a');

        // Вот таким нехитрым способом останавливаем переход по ссылкам:
        for(var i = 0, l = links.length; i < l; i++)
            links[i].onclick = function(){
                return false;
            };

        // А теперь делаем, что нам надо. Например:
        div.onclick = function(event){

// Берем кроссбраузерно событие и элемент, на котором оно произошло:
            if(!event) event = window.event;
            var target = event.target || event.srcElement;

// Проверяем, по ссылки ли кликнули:
            if(target.tagName.search(/a/i) === -1) return;

// Ну и собственно, вот ради чего все это:)
            if( confirm('Вы действительно хотите перейти по ссылке: ' + target.href) )
                window.open(target.href,'_blank');



        };
    </script>
{/literal}

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h3>Православный календарь</h3>
            <div id="pravCalendar">
                <h5>{$calendar.day}</h5>
                <p class='g-color-gray-light-v1'>{$calendar.ned} {$calendar.glas}
                {if isset($calendar.post)}
                    <br>{$calendar.post}
                {/if}
                </p>
                <p>
                    <img src='{$calendar_trapeza_img}' align='left' width='117' class='g-pr-5'>
                    {$calendar.trapeza_txt}
                </p>
                <p>{$calendar.para}</p>
                <h5>Чтения дня</h5>
                <p>{$calendar.chten}</p>
                <h5>Икона дня</h5>
                <p><img src='{$calendar_dayicon}' class='g-pr-5'> <br>{$calendar.dayicon_text}</p>
                {literal}
                    <script>
                        var div = document.getElementById('pravCalendar'),
                                links = div.getElementsByTagName('a');
                        for(var i = 0, l = links.length; i < l; i++)
                            links[i].onclick = function(){
                                return false;
                            };
                        div.onclick = function(event){
                            if(!event) event = window.event;
                            var target = event.target || event.srcElement;
                            if(target.tagName.search(/a/i) === -1) return;
                            if( confirm('Вы действительно хотите перейти по ссылке: ' + target.href) )
                                window.open(target.href,'_blank');
                        };
                    </script>
                {/literal}

            </div>
            <h3 class="g-mt-30">Фотогалерея</h3>
            <div class="row">
                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/1.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/2.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/3.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/4.jpg" alt="example-image">
                        </a>
                    </div>
                </div>
            </div>

            <p class="small text-right g-mb-30"><a href="/gallery/">все фото</a></p>
        </div>
        <div class="col-md-8">
            <h3>Новости прихода</h3>
            <div class="row">
                <div class="col-md-6">
                    <!-- Article -->
                    <div class="row g-mx-5--sm g-mb-30">
                        <!-- Article Image -->
                        <div class="col-sm-5 g-px-0--sm">
                            <div class="g-height-150 g-bg-size-cover" style="background-image: url(/uploads/den_molodeji_1.jpg);"></div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="col-sm-7 g-px-0--sm">
                            <div class="u-info-v1-1 g-height-150 g-bg-gray-light-v5 g-bg-gray-light-gradient-v1--after g-pa-10">
                                <h3 class="h6 g-font-weight-600 text-uppercase small">
                                    <a class="u-link-v5 g-color-black g-color-primary--hover" href="/news/?id=4">День православной молодёжи.</a>
                                </h3>

                                <p class="small g-color-primary">
                                    <time>18 февраля 2019</time>
                                </p>

                                <p class="small">15 февраля отмечается Всемирный день православной молодёжи. В Жуковском благочинии состоялся целый ряд мероприятий, посвящённых этому празднику. </p>
                            </div>
                        </div>
                        <!-- End Article Content -->
                    </div>
                    <!-- End Article -->
                </div>

                <div class="col-lg-6">
                    <!-- Article -->
                    <div class="row g-mx-5--sm g-mb-30">
                        <!-- Article Image -->
                        <div class="col-sm-5 g-px-0--sm">
                            <div class="g-height-150 g-bg-size-cover" style="background-image: url(/uploads/prestolniy_prazdnik.jpg);"></div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="col-sm-7 g-px-0--sm">
                            <div class="u-info-v1-1 g-height-150 g-bg-gray-light-v5 g-bg-gray-light-gradient-v1--after g-pa-10">
                                <h3 class="h6 g-font-weight-600 text-uppercase small g-pt-2">
                                    <a class="u-link-v5 g-color-black g-color-primary--hover" href="/news/?id=5">Престольный праздник.</a>
                                </h3>

                                <p class="small g-color-primary">
                                    <time>12 февраля 2019</time>
                                </p>

                                <p class="small">10 февраля, в день памяти новомучеников и исповедников Церкви Русской, в Преображенском храме г. Жуковского состоялся престольный праздник.</p>
                            </div>
                        </div>
                        <!-- End Article Content -->
                    </div>
                    <!-- End Article -->
                </div>
            </div>
            <p class="small text-right g-mb-30"><a href="/news/">все новости</a></p>




            <h3>История храма</h3>
            <figure class="sign-right">
                <p><img src="/assets/pic/photogallery/0.jpg" width="250"></p>
                <figcaption>Каменный храм в честь Рождества Иоанна Предтечи, разрушенный в 40-е годы XX века.</figcaption>
            </figure>
            <p>Вблизи озера Глушица через Москву-реку пролегал древнейший путь, освоенный, судя по остаткам городищ, еще в I тысячелетии до н. э. После присоединения Коломны к Московскому княжеству он становится одним из важнейших в экономическом и стратегическом отношении. Этот путь стал называться Астраханским трактом. В 1581 г., после смерти сына, Иван Грозный повелел поставить здесь монастырь в честь Рождества Иоанна Предтечи и устроить переправу через реку. Монахами в новом монастыре стали бывшие опричники. В писчей книге Приказа большого двора за 1631-1633 гг. указано: «За озером Здегошем, на монастырской земле стоял храм Рождества Иоанна Предтечи, храм великомученицы Параскевы, нарицаемой Пятницы, да придел Николая Чудотворца, по прошествии времени монастырь от разорения запустел». Заброшенные монастырские земли были переданы Патриарху Филарету и вскоре перешли в Дворцовое ведомство. На месте монастыря возникло село Новорождественное. Ни монастырь, ни его старые храмы не были восстановлены. В селе заново отстроили деревянный храм в честь Рождества Иоанна Предтечи. В 1710 г. Петр I подарил село Новорождественное и Раменскую волость графу И.А.Мусину-Пушкину в награду за подавление восстания в Астрахани в 1722 г., пожаловав его в вечное владение. В 1730 г. помещик возвел двухэтажную каменную церковь в честь Рождества Иоанна Предтечи, нижний придел которой освятили во имя святителя Николая. Имелось богатейшее мраморное Распятие. Среди многих старинных икон особо почиталась древняя Новодворская икона Божией Матери, в серебряной ризе. Раньше она принадлежала старинному роду князей Хромых-Волконских-Орловых. Церковный хор славился в округе. В 1852 г. село стало самым крупным в имении князей Волконских.</p>
            <p class="small text-right"><a href="history/">читать дальше...</a></p>




            <blockquote class="lead u-blockquote-v1 rounded g-pl-60 g-pr-30 g-py-30 g-mb-40 small">{$calendar.feofan}</blockquote>
            <div class="media">
                <img class="d-flex align-self-center rounded-circle g-brd-around g-brd-3 g-brd-white g-width-50 mr-3" src="/assets/pic/st_feofan.jpg" alt="свт. Феофан Затворник">
                <div class="media-body align-self-center">
                    <h4 class="h6 g-font-weight-700 g-mb-0">"Мысли на каждый день года"</h4>
                    <em class="g-color-gray-dark-v4 g-font-style-normal">свт. Феофан Затворник</em>
                </div>
            </div>


        </div>
    </div>
</div>