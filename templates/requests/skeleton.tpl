<script>
    var fs = "{$prefix}";

</script>

<div class="container g-mt-100">
    <h1>Требы онлайн</h1>
    <div class="row">
        <div class="col-md-6">
            <h5 class="successContent text-success">
                <i class="fa fa-check left text-success"></i>Ваше сообщение было успешно отправлено.
            </h5>
            <h5 class="errorContent text-danger">
                <i class="fa fa-exclamation-circle left text-danger"></i>При отправке сообщения возникла проблема!
            </h5>
            <div class="formArea">
            <form id="sendMessage" name="sendMessage" role="form" class="g-pa-30 g-mb-30 form-control-md">

                <div class="form-group g-mb-20">
                    <label class="g-mb-10" for="{$prefix}-name">Имя: </label>
                    <input id="{$prefix}-name" name="{$prefix}-name" class="form-control form-control-md rounded-0" type="text" placeholder="Введите ваше имя" required />
                </div>

                {*<div class="form-group g-mb-20">
                    <label class="g-mb-10" for="{$prefix}-email">Email: </label>
                    <input id="{$prefix}-email" name="{$prefix}-email" class="form-control form-control-md rounded-0" type="text" placeholder="Введите ваш email" >
                </div>*}

                <label class="form-check-inline u-check g-pl-25 ml-0 g-mr-25">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" id="{$prefix}-type1" name="{$prefix}-type" type="radio" value="О здравии" checked="">
                    <div class="u-check-icon-radio-v4 g-absolute-centered--y g-left-0 g-width-18 g-height-18">
                        <i class="g-absolute-centered d-block g-width-10 g-height-10 g-bg-primary--checked"></i>
                    </div>
                    О здравии
                </label>

                <label class="form-check-inline u-check g-pl-25 ml-0 g-mr-25">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" id="{$prefix}-type2" name="{$prefix}-type" type="radio"  value="О упокоении">
                    <div class="u-check-icon-radio-v4 g-absolute-centered--y g-left-0 g-width-18 g-height-18">
                        <i class="g-absolute-centered d-block g-width-10 g-height-10 g-bg-primary--checked"></i>
                    </div>
                    О упокоении
                </label>


                <div class="form-group g-mb-20">
                    <label class="g-mb-10" for="{$prefix}-email">Записка<sup*</sup>: </label>
                    <textarea  id="{$prefix}-message" name="{$prefix}-message" class="form-control form-control-md g-resize-none rounded-0" rows="5" placeholder="Текст записки"  required></textarea>
                    <small class="form-text text-muted g-font-size-default g-mt-10">
                    </small>
                </div>

                <button class="btn-outline-primary" type="submit">Отправить</button>


            </form>
            </div>
        </div>
        <div class="col-md-6">
            <h3>Дорогие братья и сестры!</h3>
            <p>Если вы хотите заказать поминовение о здравии или упокоении православных христиан, но у вас нет возможности прийти в храм, вы можете подать записку через наш сайт.<br><br>
                Записки будут прочитаны за богослужением Иоанно-Предтеченского храма.</p>
            <p>Виды поминовения:</p>
            <ul>
                <li>Простая записка о здравии/упокоении</li>
                <li>Заказная записка о здравии/упокоении</li>
            </ul>
            <p>Пожертвования на нужды нашего храма Вы тоже можете делать дистанционно. Как это сделать, вы можете узнать на странице <a href="/charity/">Помочь храму</a></p>
            <p>
                Пожертвования на нужды нашего храма Вы тоже можете делать дистанционно через сервис «Сбербанк Онлайн»:<br>
                Перевод на карту Сбербанка 5469 4000 3382 3191<br>

                Пожалуйста, указывайте слово «Пожертвование» в комментарии к переводу.
            </p>
            <p class="small text-muted"><sup*</sup>Церковная записка - разовый помянник "О здравии" или "О упокоении", подаваемый в храме для поминовения на проскомидии или ектении, на молебнах и панихидах. </p>
        </div>
    </div>
</div>