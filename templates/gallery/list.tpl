<div class="container g-mt-100">
    <h2>Жизнь храма</h2>
    <div class="row">
        {foreach $pics as $pic}
            <div class='col-md-2 g-mb-30--md'>
                <div class='g-brd-around g-brd-gray-light-v4--hover'>
                    <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/full/{$pic}' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                        <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/{$pic}'>
                    </a>
                </div>
            </div>
        {/foreach}

    </div>

    {$pagination}

</div>