<ul class="list-unstyled">
    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="pottery">Гончарное ремесло</a></h5>
        </div>
        <p>Не много людей откажется от предложения сесть за гончарный круг и  попробовать собственными руками смастерить пусть даже неказистый горшочек из глины...</p>
        <div class="d-flex justify-content-between">
            <ul class="u-list-inline">
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/pottery/thumb/1.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/pottery/thumb/2.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/pottery/thumb/3.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <a class="u-link-v4 d-inline-block g-bg-blue g-color-white g-font-weight-600 g-text-underline--none--hover g-rounded-50 g-py-7 g-px-10" href="pottery">...</a>
                </li>
            </ul>
        </div>
    </li>

    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="smithy">Кузня</a></h5>
        </div>
        <p>Изделия художественной ковки украшают обыкновенные дома, дворцы, замки, храмы, придавая строениям легкость и изящество...</p>
        <div class="d-flex justify-content-between">
            <ul class="u-list-inline">
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/smithy/thumb/1.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/smithy/thumb/2.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/smithy/thumb/3.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <a class="u-link-v4 d-inline-block g-bg-blue g-color-white g-font-weight-600 g-text-underline--none--hover g-rounded-50 g-py-7 g-px-10" href="smithy">...</a>
                </li>
            </ul>
        </div>
    </li>

    <li class="g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-blue-left g-rounded-3 g-pa-20 g-mb-7">
        <div class="d-flex justify-content-start">
            <h5 class="g-font-weight-600 g-color-black"><a href="painting">Мезенская живопись</a></h5>
        </div>
        <p>Каждая деталь орнамента мезенской росписи глубоко символична. Она источает добро и мудрость предков. Каждый квадратик и ромбик, зверь и птица, листик и веточка находятся именно в том месте, где они должны быть...</p>
        <div class="d-flex justify-content-between">
            <ul class="u-list-inline">
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/painting/thumb/1.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/painting/thumb/2.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <img class="g-brd-around g-brd-gray-light-v3 g-pa-2 g-width-40 g-height-40 rounded-circle" src="/assets/pic/painting/thumb/3.jpg">
                </li>
                <li class="list-inline-item mr-0">
                    <a class="u-link-v4 d-inline-block g-bg-blue g-color-white g-font-weight-600 g-text-underline--none--hover g-rounded-50 g-py-7 g-px-10" href="painting">...</a>
                </li>
            </ul>
        </div>
    </li>
</ul>