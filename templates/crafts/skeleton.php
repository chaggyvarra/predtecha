<div class="container g-mt-100">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-unstyled">
                <li><a href="pottery" class="g-color-gray-dark-v1 g-font-size-18--md">Гончарное ремесло</a></li>
                <li><a href="smithy" class="g-color-gray-dark-v1 g-font-size-18--md">Кузня</a></li>
                <li><a href="painting" class="g-color-gray-dark-v1 g-font-size-18--md">Мезенская роспись</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <?php include($rightColumnPage);?>
        </div>
    </div>
</div>