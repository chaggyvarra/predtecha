<h1>Гончарное ремесло</h1>
<p>Гончарное дело относится к самым древним видам ремесла и интерес к нему не исчезает и в современном мире. Не много людей откажется от предложения сесть за гончарный круг и  попробовать собственными руками смастерить пусть даже неказистый горшочек из глины, а что говорить о детях, которые с удовольствием возятся с этим благодатным природным материалом, сколько радости и эмоций! В Иоанно-Предтеческом приходе на территории храма, есть небольшая гончарная мастерская, где мастера гончарного дела Дмитрий Язов и Светлана Месяцева не только познакомят с производством изделия, но и предоставят возможность желающим самим побыть в роли гончара. </p>

<div class="row">
    <?php
    $dir = opendir('./assets/pic/pottery/');
    $count = 0;
    while($file = readdir($dir)){
        if($file == '.' || $file == '..' || is_dir('./assets/pic/pottery/' . $file)){
            continue;
        }
        $count++;
    }
    //$count = 20;
    $i = 1;
    do {
        echo "<div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/pottery/$i.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/pottery/thumb/$i.jpg'>
            </a>
        </div>
    </div>";
        $i++;
    }
    while ($i <= $count);
    ?>
</div>