<div class="container g-mt-100">
    <h1>Настоятель Иоанно-Предтеченского храма</h1>
      <div class="row">
          <div class="col-md-4">
              <p><b>Священник Сергий Петрович Симаков, 1959<br><br>
                    Член Миссионерско-молодёжного отдела Коломенской епархии (ответственный за взаимодействие с обществом и СМИ от Жуковского благочиннического округа) ответственный за экологическую работу в Жуковском благочинии.<br>
                    Настоятель Иоанно-Предтеченского храма, г.о. Жуковский<br><br>
                  <b>Светское образование</b><br>
                  высшее 1982; МАИ<br><br>
                  <b>Духовное образование</b><br>
                  нет<br><br>
                  <b>Хиротонисан</b><br>
                  27 января 2008 года<br>
                  архиепископом Можайским Григорием в Успенском храме Новодевичьего монастыря г. Москвы<br><br>
                  <b>День тезоименитства</b><br>
                  8 октября<br><br>
                  <b>Награды</b><br>
                  <b>2021</b> камилавка<br>
                  <b>2017</b> набедренник<br>
                  <b>2010</b> грамота за усердные труды на ниве духовно-нравственного просвещения и образования</p>
          </div>
          <div class="col-md-8">
              <div class="row">
                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/1.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/2.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/3.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/4.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/5.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/5.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/6.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/6.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/7.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/7.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/8.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/8.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/9.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/9.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/10.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/10.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/11.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/11.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/12.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/12.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/13.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/13.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/14.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/14.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/15.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/15.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/16.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/16.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/17.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/17.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>

                  <div class="col-md-3 g-mb-30--md">
                      <div class="g-brd-around g-brd-gray-light-v4--hover">
                          <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/prior/18.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                              <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/prior/thumb/18.jpg" alt="example-image">
                          </a>
                      </div>
                  </div>


              </div>
          </div>
      </div>
</div>