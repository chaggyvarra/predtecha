<div class="container g-mt-100">
    <h1>Музей села Новорождествено</h1>
    <p>При  раскопках и поиске фундамента разрушенного в 40–е годы прошлого века Иоанно Предтеченского храма, были найдены историко-археологические предметы: оклады от уничтоженных икон, церковная утварь, фрагменты кованых украшений бывшего храма, сельскохозяйственные и бытовые орудия труда, предметы  домашнего быта, старинная посуда и монеты.<br>
        Небольшая экспозиция музея дает представление об укладе жизни проживающих раннее на этой территории людей.
    </p>
    <div class="g-mt-60">
        <div class="row">
            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/1.jpg" alt="example-image">
                    </a>
                </div>
            </div>

            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/2.jpg" alt="example-image">
                    </a>
                </div>
            </div>

            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/3.jpg" alt="example-image">
                    </a>
                </div>
            </div>

            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/4.jpg" alt="example-image">
                    </a>
                </div>
            </div>

            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/5.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/5.jpg" alt="example-image">
                    </a>
                </div>
            </div>

            <div class="col-md-2 g-mb-30--md">
                <div class="g-brd-around g-brd-gray-light-v4--hover">
                    <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/museum/6.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/museum/thumb/6.jpg" alt="example-image">
                    </a>
                </div>
            </div>


        </div>
    </div>
</div>