<div class="container g-mt-100">
    <h1>Святыни</h1>
    <p></p>
    <div class="row">
        <div class="col-md-2 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/shrines/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Тихвинская икона Божьей матери">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/shrines/thumb/1.jpg" alt="example-image">
                </a>
            </div>
            <p class="small"><b>Тихвинская икона Божьей матери</b><br>В 2001 году приходу были пожертвованы две иконы: Тихвинская икона Божьей матери и Господь Вседержитель. Владелец, пожертвовавший иконы, пожелал остаться тайным. Тихвинская икона Божьей матери хорошо сохранилась, а вот вторая сильно пострадала и сейчас находится на реставрации в приходе целителя Пантелеймона города Жуковского.</p>
        </div>

        <div class="col-md-2 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/shrines/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Моление о чаше">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/shrines/thumb/2.jpg" alt="example-image">
                </a>
            </div>
            <p class="small"><b>Моление о чаше</b><br>Икона, спасенная верующими от разорения во времена богоборчества в России в XX веке, из церкви села Каменское Нарофоминского района. Дарительница - Лидия Васильевна Орлова, урожденная Стрельникова.</p>
        </div>

        <div class="col-md-2 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/shrines/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Рождество Иоанна Предтечи">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/shrines/thumb/3.jpg" alt="example-image">
                </a>
            </div>
            <p class="small"><b>Рождество Иоанна Предтечи</b> подарена жителями села Новорождественное</p>
        </div>

        <div class="col-md-2 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/shrines/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Иоанн Предтеча">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/shrines/thumb/4.jpg" alt="example-image">
                </a>
            </div>
            <p class="small"><b>Иоанн Предтеча</b>  подарена жителями села Новорождественное</p>
        </div>
    </div>
</div>