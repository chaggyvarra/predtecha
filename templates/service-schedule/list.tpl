<div class="container g-mt-100">
    <h2>{$monthName}</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <tbody>
            {foreach $schedule as $item}
                <tr>
                    {if $item.weekDay == 7}
                        <td class='g-color-red g-font-weight-700'>{$item.day}<br>{$item.weekDay_txt.short}</td>
                    {else}
                        <td>{$item.day}<br>{$item.weekDay_txt.short}</td>
                    {/if}
                    <td>{$item.time}</td>
                    <td>{$item.col1}</td>
                    <td>{$item.col2}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>