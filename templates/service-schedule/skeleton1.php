<div class="container g-mt-100">

<div class="row">
    <div class="col-md-11">
        <h2>Январь 2020</h2>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                    <td>1<br>СР</td>
                    <td>0:00</td>
                    <td>Часы. Литургия.</td>
                    <td>Мч. Вонифатия. Прп. Илии Муромца</td>
                </tr>
                <tr>
                    <td rowspan="2">4<br>СБ</td>
                    <td>8:00</td>
                    <td>Панихида. Утреня. Часы. Литургия.</td>
                    <td rowspan="2">Суббота пред Рождеством
                        Христовым. Вмц. Анастасии
                        Узорешительницы</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Всенощное бдение. 1-й час.</td>
                </tr>
                <tr>
                    <td rowspan="2" class="g-color-red g-font-weight-700">5<br>ВС</td>
                    <td>9:30</td>
                    <td>Часы. Литургия.</td>
                    <td rowspan="2">Неделя 29-я по Пятидесятнице,
                        пред Рождеством Христовым</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Вечерня.</td>
                </tr>
                <tr>
                    <td rowspan="2">6<br>ПН</td>
                    <td>8:00</td>
                    <td>Часы навечерия (Царские часы).
                        Изобразительны.
                        Литургия свт.Василия Великого.</td>
                    <td rowspan="2">Навечерие Рождества Христова
                        (Рождественский сочельник)</td>
                </tr>
                <tr>
                    <td style="border: 0;">22:00</td>
                    <td style="border: 0;">9-й час. Всенощное бдение. Часы.</td>
                </tr>
                <tr>
                    <td rowspan="2" class="g-color-red g-font-weight-700">7<br>ВТ</td>
                    <td>0:00</td>
                    <td>Часы. Литургия свт. Иоанна Златоуста.</td>
                    <td rowspan="2" class="g-color-red g-font-weight-700">РОЖДЕСТВО ГОСПОДА БОГА И СПАСА
                        НАШЕГО ИИСУСА ХРИСТА</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Великая вечерня.</td>
                </tr>
                <tr>
                    <td>8<br>СР</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия. </td>
                    <td>Собор Пресвятой Богородицы</td>
                </tr>
                <tr>
                    <td rowspan="2">11<br>СБ</td>
                    <td>8:00</td>
                    <td>Панихида. Утреня. Часы. Литургия.</td>
                    <td rowspan="2">Суббота по Рождестве Христовом</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Всенощное бдение. 1-й час</td>
                </tr>
                <tr>
                    <td rowspan="2" class="g-color-red g-font-weight-700">12<br>ВС</td>
                    <td>9:30</td>
                    <td>Часы. Литургия.</td>
                    <td rowspan="2">Неделя 30-я по Пятидесятнице, по
                        Рождестве Христовом. Прав. Иосифа
                        Обручника, Давида царя и Иакова,
                        брата Господня</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Вечерня</td>
                </tr>
                <tr>
                    <td>14<br>ВТ</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия свт. Василия
                        Великого</td>
                    <td>Обрезание Господне. свт. Василия
                        Великого архиеп. Кесарии
                        Каппадокийской</td>
                </tr>
                <tr>
                    <td>15<br>СР</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия.</td>
                    <td>Прп. Серафима Саровского</td>
                </tr>
                <tr>
                    <td>17<br>ПТ</td>
                    <td>8:00</td>
                    <td>Часы навечерия (Царские часы).
                        Изобразительны. Литургии не положено</td>
                    <td>Собор 70-ти апостолов</td>
                </tr>
                <tr>
                    <td rowspan="2">18<br>СБ</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия свт. Иоанна
                        Златоуста. 9-й час. Великая вечерня.</td>
                    <td rowspan="2">Суббота пред Богоявлением.
                        (Крещенский сочельник). Великое
                        водоосвящение</td>
                </tr>
                <tr>
                    <td style="border: 0;">22:00</td>
                    <td style="border: 0;">9-й час. Всенощное бдение. Часы.</td>
                </tr>
                <tr>
                    <td rowspan="2" class="g-color-red g-font-weight-700">19<br>ВС</td>
                    <td>0:00</td>
                    <td>Часы. Литургия свт. Василия Великого.</td>
                    <td rowspan="2" class="g-color-red g-font-weight-700">БОГОЯВЛЕНИЕ. КРЕЩЕНИЕ ГОСПОДА БОГА И СПАСА НАШЕГО ИИСУСА ХРИСТА</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Великая вечерня.</td>
                </tr>
                <tr>
                    <td>20<br>ПН</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия.</td>
                    <td>Собор Предтечи и Крестителя Господня Иоанна</td>
                </tr>
                <tr>
                    <td rowspan="2">25<br>СБ</td>
                    <td>8:00</td>
                    <td>Панихида. Утреня. Часы. Литургия.</td>
                    <td rowspan="2">Суббота по Богоявлении. Мц. Татианы</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Всенощное бдение.</td>
                </tr>
                <tr>
                    <td rowspan="2" class="g-color-red g-font-weight-700">26<br>ВС</td>
                    <td>9:30</td>
                    <td>Часы. Литургия.</td>
                    <td rowspan="2">Неделя 32-я по Пятидесятнице, по Богоявлении</td>
                </tr>
                <tr>
                    <td style="border: 0;">17:00</td>
                    <td style="border: 0;">9-й час. Вечерня.</td>
                </tr>
                <tr>
                    <td>27<br>ПН</td>
                    <td>8:00</td>
                    <td>Утреня. Часы. Литургия.</td>
                    <td>Отдание Богоявления. Св.равноап. Нины, просветительницы Грузии</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
    <div style="display: none" class="col-md-2 offset-md-1">
        <div id="calendar"></div>
    </div>
</div>
</div>