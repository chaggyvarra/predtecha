<div class="container g-mt-100">
    <h1>Храм за озером Здегошем</h1>

    <div class="row">
        <div class="col-md-8">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/8A71fA8GE34" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4">
            Над фильмом работали:<br>
            Съемка и монтаж - <b>Борис Зайчик</b><br>
            Музыка -  <b>Вокальный ансамбль ЕЛЕЙ</b><br><br>

            В фильме принимали участие:<br>
            Настоятель Иоанно-Предтеченского храма <b>иерей Сергей Симаков</b><br>
            Помощник настоятеля Иоанно-Предтеченского храма <b>Дмитрий Язов</b><br>
            Регент Иоанно-Предтеченского храма <b>Светлана Месяцева</b><br>
            Руководитель церковного историко-археологического кабинета г.Жуковский <b>Наталья Богословская</b><br>
        </div>
    </div>

</div>