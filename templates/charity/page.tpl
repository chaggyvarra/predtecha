<div class="container g-mt-100">
    <h1>Жертвователям</h1>
    <p>
        Дорогие благотворители! Приход существует на ваши пожертвования, в которых мы нуждаемся. Мы будем рады даже скромным пожертвованиям, но сделанным от всего сердца во славу Господа нашего Спасителя.
        <br>
        Во время каждого богослужения священнослужители и прихожане молятся о благоустроителях и жертвователях храма.
        <br>
        Приходу необходимы пожертвования на строительство храма в честь Новомучеников Жуковских (Новорождественских), который будет располагаться неподалёку от храма в честь Рождества Иоанна Предтечи (р-н Наркомвод г.Жуковский) – того места, где раньше жили и служили пострадавшие за веру Христову новомученики.
        <br>
        Также приходу необходимы пожертвования для содержания и благоустройства храма и его территории, помощи нуждающимся. Сердечно благодарим всех, кто откликается и помогает! Помните, что «рука дающего не оскудеет». Храни Вас Господь!
    </p>

   <p>
        <b class="h3">Способ 1.</b><br>
        Пожертвовать на деятельность храма вы можете путем перевода на указанную карту Сбербанка:<br>
        <span class="numbers">5469 4000 3382 3191</span>  <br>
        Получатель: Мария Алексеевна Г.

    </p>

    <p>
        <b class="h3">Способ 2.</b><br>

        Внести средства на счёт прихода Вы можете, распечатав квитанцию  и оплатив в любом отделении Сбербанка,

        с обязательным указанием платежа: <b>«Пожертвование на ведение уставной деятельности».</b>
        <br><br>

        <a href="/assets/data/kvitancia.pdf" target="_blank">Квитанция Сбербанка</a> <sup class="g-font-size-12">*откроется в новой вкладке</sup> <br><br>
    </p>

    <p>
        <b class="h3">Способ 3.</b><br>

        По реквизитам храма через онлайн или личный кабинет любого банка, в разделе платежи или переводы, совершить перевод с обязательным указанием назначения платежа: <b>«Пожертвование на ведение уставной деятельности»</b>.
        <br><br>
        Реквизиты для перевода:<br>

        Местная религиозная организация
        православный приход Иоанно-Предтеченского храма<br>
        г. Жуковского Московской области<br>
        Коломенской епархии Русской Православной Церкви <br>
        ОГРН: 1035000033188<br>
        ИНН: 5013039558 <br>
        КПП: 501301001 <br>
        Наименование банка: Филиал «Центральный» Банка ВТБ (ПАО) в г. Москве<br>
        БИК: 044525411  <br>
        к/с: 30101810145250000411 <br>
        р/с: 40703810116030000011
    </p>

    <p>
        <b class="h3">Способ 4.</b><br>
        Вы можете сделать пожертвование в вашем онлайн-банке. Для этого вам следует открыть ваше банковское приложение, найти кнопку «Оплатить по QR-коду» и отсканировать камерой вашего смартфона расположенный ниже код. В открывшейся форме перевода следует указать сумму пожертвования.
    </p>
    <div><img src="/assets/pic/charity/qr_code_1.svg" class="offset-md-3 col-md-6"></div>

    <p>
        <b class="h3">Способ 5.</b><br>
        Также Вы можете внести свои пожертвования, опустив их в ящики для сбора в храмах прихода.
    </p>

    <p>Благодарим Вас за оказанную помощь! Храни Вас Господь!</p>




</div>