<div class="container g-mt-100">
    <h1>Раскопки и строительство нового храма</h1>

    <div class="row">
        <?php
        $dir = opendir('./assets/pic/novadays/');
        $count = 0;
        while($file = readdir($dir)){
            if($file == '.' || $file == '..' || is_dir('./assets/pic/novadays/' . $file)){
                continue;
            }
            $count++;
        }
        //$count = 20;
        if ($count > 0) {
        $i = 1;
        do {
            echo "<div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/novadays/$i.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/novadays/thumb/$i.jpg'>
            </a>
        </div>
    </div>";
            $i++;
        }
        while ($i <= $count);
        }
        ?>
    </div>

</div>