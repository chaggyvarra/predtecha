<div class="container g-mt-100">
    <h1>Козья ферма</h1>
    <p>Территория Иоанно-Предтеченского храма находится почти в черте города, но место это окруженное водой в виде полуострова, необыкновенно тихое и благодатное, время там течет в своем ритме невзирая на динамику города. На этом не подвластном времени островке живет небольшое стадо коз, они очень добродушные и общительные, всегда рады новым посетителям, а еще они щедрые и с удовольствием делятся своим вкуснейшим и экологически чистым молоком!</p>

    <div class="row">
        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/1.jpg" alt="example-image">
                </a>
            </div>
        </div>

        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/2.jpg" alt="example-image">
                </a>
            </div>
        </div>

        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/3.jpg" alt="example-image">
                </a>
            </div>
        </div>

        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/4.jpg" alt="example-image">
                </a>
            </div>
        </div>

        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/5.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/5.jpg" alt="example-image">
                </a>
            </div>
        </div>

        <div class="col-md-1 g-mb-30--md">
            <div class="g-brd-around g-brd-gray-light-v4--hover">
                <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/goat-farm/6.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/goat-farm/thumb/6.jpg" alt="example-image">
                </a>
            </div>
        </div>
    </div>



</div>
