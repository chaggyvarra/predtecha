<?php


echo '<script> var a = '.(int)$balance.'</script>';
?>
<script src="/assets/vendor/chartjs/Chart.min.js"></script>
<script src="/assets/vendor/chartjs/samples/utils.js"></script>
<script>
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    //var a = randomScalingFactor() * 10000;
    //var b = randomScalingFactor() * 10000;
    //var a = 116200;

    var c = 1500000;
    var b = c - a;

    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    a,
                    b
                ],
                backgroundColor: [
                    window.chartColors.green,
                    window.chartColors.grey
                ],
                label: 'Пожертвовали'
            }],
            labels: [
                'Собрано',
                'Осталось'
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Внесено пожертвований:'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById('chart-area').getContext('2d');
        window.myDoughnut = new Chart(ctx, config);
        document.getElementById('chart-label').innerHTML="Cобрано средств: <b>" + a + "₽</b><br>Осталось собрать: <b>" + b + "₽</b><br>Cтоимость проекта: <b>" + c +"₽</b>";
        document.getElementById('leftsum').innerHTML="Осталось собрать: <b>" + b + "₽</b>";
    };






</script>
<div class="container g-mt-100">
    <div class="row">
        <div class="col-md-8">
            <h2>Реконструкция и расширение гостевого дома-трапезной.</h2>
            <p class="g-width-300 g-mb-30 g-font-style-italic">«Если каждый человек на куске земли своей сделал бы всё, что он может, как прекрасна была бы земля наша!»<br><span class="float-right">Антон Чехов</span></p>

            <div class="embed-responsive embed-responsive-16by9 g-mb-30">
                <iframe width="100%" src="https://www.youtube.com/embed/mi7MRF7bqCw" frameborder="0" allowfullscreen=""></iframe>
            </div>

            <div class="g-hidden-sm-up">
                <p>Дорогие братья и сестры! С Божией помощью отступают временные трудности с ограничением посещения храмов. Мы постепенно возвращаемся к привычной жизни и к планам, которые были запланированы ранее.<br>

                    В конце марта, должен был стартовать проект для привлечения общественного внимания на остро возникшую проблему, по расширению и реконструкции гостевого дома трапезной на территории Иоанно - Предтеченского храма.<br>

                    Два сообщества «Жуковское общество трезвости» и социально инициативный проект «Начни с себя» объединились, под непосредственным окормлением настоятеля прихода иереем Сергием Симаковым, для осуществления и реализации этих планов.<br>

                    Совсем небольшая трапезная, в первую очередь используется, для благотворительных обедов на пожертвования. Община оказывает помощь людям оказавшимся в затруднительной жизненной ситуации. Устраиваются лекции, турниры, конкурсы, летние лагеря для детей из многодетных семей, вело пробеги за трезвый образ жизни, ярмарки народных промыслов.<br>

                    Трапезная несет большую социальную нагрузку.<br>

                    Для всех запланированных мероприятий она становится тесной, количество желающих участников растет. Быстро и качественно своими силами, решить проблему реконструкции, не возможно, поэтому мы обращаемся к уважаемым инвесторам, жертвователям, спонсорам, к социально активным людям принять горячее участие в осуществлении столь жизненно необходимого проекта.<br>

                    Необходимые расходы составляют 1 500 000 р.<br>

                    Мы приглашаем, всех в гости, и как гостей и как участников всей нашей общественной жизни! Двери всегда открыты мы ждем вас!</p>
            </div>

            <div class="g-hidden-sm-down"><p class="text-justify">Две социально-культурные организации «Жуковское общество трезвости» и социально инициативный проект «Начни с себя» объединились для реализации проекта, реконструкции и расширения небольшого, но очень востребованного гостевого дома и трапезной, вблизи г. Жуковского, на территории Иоанно — Предтеченского храма.<br>

                Рано или поздно каждый человек задает себе или другим вопрос: «Что хорошего и полезного сделал в жизни?»<br>

                Действие одного человека может многое изменить, если что-то хорошее может сделать кто угодно, почему бы это не сделать всем нам.<br>

                Далеко не все могут в короткие сроки реализовывать грандиозные планы или проекты, и не всем это нужно и полезно. Усилие одного человека имеет значение, начав изменения с себя, перемены становятся очевидными и для окружающих, многие будут считать вас странными, а кто-то возьмет с вас пример. Очень важно научиться искусству маленьких шагов, использовать и реализовать те возможности, которые вы можете.<br>

                Начать с себя, маленькой точки, от которой будут расходиться круги созидания лучшего, направленные на пути добра, гуманизма, человеческих взаимоотношений, дружбы, взаимопонимания, творчества и любви.<br>

                Не достаточное внимание общества к проблемам всеобщей алкогольной зависимости, табакокурению, наркомании, эти страшные недуги не искореняются, а порой широко рекламируются, как стильный, благополучный, респектабельный образ жизни, не причиняющий никакого вреда здоровью человека. Наши дети стали постоянными созерцателями распития алкогольных напитков и табакокурения взрослыми, как дома, так и в общественных местах. Мы искренне верим, что ничего плохого не делаем, предпочитая застолье, вместо прогулки с детьми, или каких либо совместных дел приучающих ребенка к труду, творчеству, созиданию.<br>

                Но не все еще потеряно и здравый смысл все-таки побеждает, люди начинают понимать, что невежество и нигилизм в сочетании с потребительской психологией становятся опасными для существования всего живого на Земле, в первую очередь для самого человека!<br>

                Все больше и больше людей начинают объединяться в разные социальные сообщества по интересам, где предоставляется возможность не только получить, но и оказать помощь другим.<br>

                Два таких сообщества, это «Жуковское общество трезвости» и социально инициативный проект «Начни с себя», объединившись на небольшой территории Иоанно - Предтеченского храма вблизи г. Жуковского, при непосредственном окормлении настоятеля храма иерея Сергия Симакова, для привлечения общественного внимания на остро возникшую проблему, по расширению и реконструкции гостевого дома трапезной.<br>

                Для всех уже осуществленных и запланированных мероприятий она становится тесной, количество желающих участников растет. Быстро и качественно своими силами, решить эту проблему не предоставляется возможности, поэтому мы обращаемся к уважаемым инвесторам, спонсорам, к социально активным людям принять горячее участие в осуществлении столь жизненно необходимого проекта.<br>

                Трапезная в первую очередь используется, для благотворительных обедов на пожертвования, так же организуются различные тематические встречи с интересными людьми, проходят лекции, турниры и конкурсы. Община оказывает помощь людям оказавшимся в затруднительной жизненной ситуации.<br>

                Для детей организуются детские спектакли, художественные конкурсы, подвижные игры, работает воскресная школа. Летом проводится военно — патриотическое мероприятие «Чтобы помнили...» при поддержке администрации города и общественных организаций, организуются летние многодневные лагеря отдыха для многодетных семей. Проходит ежегодная летняя ярмарка народных промыслов, где для детей и взрослых устраиваются мастер классы по различным видам прикладного искусства: по техникам росписи, гончарного и кузнечного дела, ландшафтного дизайна. Так же проходит ежегодный велопробег посвященный трезвому образу жизни с количеством участников более трехсот человек. На территории храма дети и взрослые могут пообщаться с домашними животными приобрести коровье, козье молоко, яйца из под курочки! Найти и приютить домашнего питомца котенка, кролика, собаку!</p>

 <p class="text-center g-font-weight-700"> Мы приглашаем, всех в гости, и как гостей и как участников всей нашей общественной жизни!
     <br>Двери всегда открыты мы ждем вас!</p></div>
        </div>
        <div class="col-md-4 text-center">
            <div id="canvas-holder" class="g-pt-40">
                <canvas id="chart-area"></canvas>
                <p id="chart-label" class="text-center"></p>
            </div>
            <p>Пожертвовать на реконструкцию трапезной вы можете переводом на карту Сбербанка с <span class="g-text-underline">обязательным</span> комментарием «<b class="text-danger">Трапезная</b>»:<br>
                5469 4000 3382 3191<br>
                Получатель: Мария Алексеевна Г.</p>
<!--            <a class="btn btn-large btn-info js-fancybox" href="javascript:;" data-fancybox="lightbox-gallery--16" data-src="#payment-form" data-speed="350">Сделать пожертвование</a>-->

            <div id="payment-form" class="clearfix rounded-0 g-pa-30" style="display: none; min-width: 400px;">
                <h4 class="h5 text-uppercase g-letter-spacing-1 g-mb-20">Сделать пожертвование</h4>
                <p id="leftsum"></p>

                <form>
                    <div class="form-group g-mb-20">
                        <label class="g-mb-10">Введите сумму</label>
                        <div class="js-quantity input-group u-quantity-v1 g-width-250 g-brd-primary--focus">

                            <input class="form-control text-center rounded-0 g-pa-15" type="number" value="50">
                            <div class="input-group-append g-color-gray g-bg-grey-light-v3">
                                <div class="input-group-text justify-content-center g-width-55 g-font-size-16 g-color-gray g-bg-gray-light-v5 rounded-0">₽</div>

                            </div>
                        </div>
                    </div>



                    <a class="btn btn-md u-btn-primary pull-right" href="javascript:;">Пожертвовать</a>

                </form>
            </div>

<!--            <div class="row g-mt-40">-->
<!--                <div class="offset-2 col-md-8">-->
<!--                    <a class="js-fancybox" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="https://www.youtube.com/watch?v=mi7MRF7bqCw?autoplay=0" data-speed="350">-->
<!--                        <i class="fa fa-4x fa-play-circle-o"></i><br>-->
<!--                        Видео о проекте-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->

            <div class="row g-mt-40">
                <h3>Храм Рождества Иоанна Предтечи</h3>
                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/1.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/1.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/2.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/2.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/3.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/3.jpg" alt="example-image">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 g-mb-30--md">
                    <div class="g-brd-around g-brd-gray-light-v4--hover">
                        <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" href="javascript:;" data-fancybox="lightbox-gallery--17" data-src="/assets/pic/photogallery/4.jpg" data-animate-in="bounceInDown" data-animate-out="bounceOutDown" data-speed="1000" data-overlay-blur-bg="true" data-caption="Храм Рождества Иоанна Предтечи">
                            <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="/assets/pic/photogallery/4.jpg" alt="example-image">
                        </a>
                    </div>
                </div>
            </div>

            <div class="row g-mt-40">
                <h3>Жизнь нашей общины</h3>
                <?php echo $gal; ?>
            </div>



        </div>
    </div>





</div>
