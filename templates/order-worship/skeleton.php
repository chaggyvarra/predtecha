<div class="container g-mt-130">
    <h1 class="g-line-height-0_7 g-mb-30">Заказать требы<br><span class="g-color-blue g-font-size-12 text-uppercase">поминовение через сайт</span> </h1>
    <p>Братья и сестры! Если Вы не можете прийти в храм, но хотите помолиться о своих близких — отправьте записку с сайта.</p>
    <div class="row">
        <div class="col-md-6">
            <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                <div class="form-group g-mb-25">
                    <label for="worship_type">Тип записки</label>
                    <select class="form-control rounded-10" id="worship_type">
                        <option>О здравии</option>
                        <option>Об упокоении</option>
                    </select>
                </div>

                <div class="form-group g-mb-25">
                    <label for="worship_target">О ком</label>
                    <input class="form-control rounded-10" id="worship_target">
                </div>

                <div class="form-group g-mb-25">
                    <label for="worship_target">Ваша электронная почта</label>
                    <input class="form-control rounded-10" id="email" type="email">
                </div>

                <div class="form-group g-mb-25">
                    <label for="worship_target">Ваше ФИО</label>
                    <input class="form-control rounded-10" id="name" type="text">
                </div>

                <div class="form-group g-mb-25">
                    <label for="worship_target">Примечание</label>
                    <textarea class="form-control" id="comment"></textarea>
                </div>

                <div class="form-check">
                    <label class="form-check-label g-mb-20">
                        <input type="checkbox" class="form-check-input mr-1" required>Я согласен на обработку предоставленных мной персональных данных
                    </label>
                </div>

                <button type="submit" class="btn btn-md u-btn-primary rounded-0">Отправить</button>
            </form>
        </div>
        <div class="col-md-6">
            <ol class="list-cont g-line-height-1_8">
                <li class="g-pb-20">Принимаются имена только крещеных христиан православного вероисповедания.</li>
                <li class="g-pb-20">Не крещенных в Православной вере и самоубийц в записках писать нельзя!</li>
                <li class="g-pb-20">Имена пишутся в родительном падеже (пример: Василия, Елены) строго через запятую.</li>
                <li class="g-pb-20">Записки не принимаются на конкретную дату: поминовение начинается через 1 - 3 дня после получения вами подтверждающего письма.</li>
                <li class="g-pb-20">Можно заказать требы о здравии и упокоении почтой</li>
            </ol>
        </div>
    </div>





</div>