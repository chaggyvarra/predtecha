<?php
session_start();
error_reporting(0);
date_default_timezone_set('Europe/Moscow');
require_once("data/Settings.php");
require_once("utils/strings.php");
require_once 'vendor/autoload.php';
require 'vendor/smarty/smarty/libs/Smarty.class.php';

$smarty = new Smarty;

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url = ltrim($url, "/");
$url = rtrim($url, "/");
$pages = explode("/", $url);

if ($pages[0] == 'balance') {
    include("blnc/index.php");exit;
}

$meta['title']= PAGE_TITLE;
$meta['description'] = PAGE_DESCRIPTION;
$meta['keywords'] = PAGE_KEYWORDS;
$meta['robots'] = PAGE_ROBOTS;
require_once("utils/pagination.php");

$mainComponent = "home";
$component = !empty($pages[0]) ? "components/".$pages[0].".php" : "components/".$mainComponent.".php";

if (file_exists($component) == false)
    $component = "components/".$mainComponent.".php";

include ($component);



$smarty->cache_lifetime = 1;

include("templates/common/header.php");
include("templates/common/top_menu.php");


if ($pages[0] == "gallery" || $pages[0] == "news" || $pages[0] == "requests" || $pages[0] == "charity") {
    $smarty->display($pageTemplate);
}
else {
    include($pageTemplate);
}




//include($pageTemplate);

include("templates/common/footer.php");