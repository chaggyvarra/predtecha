<?php
session_start();
error_reporting(0);
require_once("data/Settings.php");
require_once 'vendor/autoload.php';



$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url = ltrim($url, "/");
$url = rtrim($url, "/");
$pages = explode("/", $url);

$meta['title']= PAGE_TITLE;
$meta['description'] = PAGE_DESCRIPTION;
$meta['keywords'] = PAGE_KEYWORDS;
$meta['robots'] = PAGE_ROBOTS;
require_once("utils/pagination.php");

$mainComponent = "home";
$component = !empty($pages[0]) ? "components/".$pages[0].".php" : "components/".$mainComponent.".php";

if (file_exists($component) == false)
    $component = "components/".$mainComponent.".php";

include ($component);





include("templates/common/header.php");
include("templates/common/top_menu.php");

include($pageTemplate);

include("templates/common/footer.php");