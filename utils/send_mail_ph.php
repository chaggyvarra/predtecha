<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require_once '../vendor/autoload.php';
require_once("strings.php");

$mail = new PHPMailer(true);
$mail->setLanguage('ru', '/vendor/phpmailer/language/');
$mail->CharSet = "utf-8";

$targetEmail = 'chaggy@mail.ru';

$name = cleanText($_REQUEST['name'], 2);
//$telegram = $_POST['telegram'];
//$email = $_POST['email'];
$message = cleanText($_REQUEST['message'], 2);
$mtype = cleanText($_REQUEST['mtype'], 2);

$body = "<h2>Записка с сайта predtecha.online</h2>
<table style='border: 0; width: 70%;'>
<tr>
<td style='width: 40%; padding: 5px;'><b>Текст записки:</b></td>
<td style='width: 60%; padding: 5px;'>".$message."</td>
</tr>
<tr>
<td style='width: 40%; padding: 5px;'><b>Тип записки:</b></td>
<td style='width: 60%; padding: 5px;'>".$mtype."</td>
</tr>
<tr>
<td style='width: 40%; padding: 5px;'><b>Отправитель</b></td>
<td style='width: 60%; padding: 5px;'>".$name."</td>
</tr>
</table>";

$altbody = "Записка: {$message} \r\n
         Тип : {$mtype} \r\n
         От : {$name} \r\n";

try {
    //Server settings
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
    $mail->isSMTP();
    $mail->Host       = 'smtp.jino.ru';
    $mail->SMTPAuth   = true;
    $mail->Username   = 'noreply@predtecha.online';
    $mail->Password   = 'e5p_jhTv[d3=';
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $mail->Port       = 465;

    $mail->setFrom('robot@predtecha.online', 'Predtecha.online');

    $mail->addAddress($targetEmail, 'отец Сергий');

    $mail->isHTML(true);
    $mail->Subject = 'Записка с сайта predtecha.online';
    $mail->Body    = $body;
    $mail->AltBody = $altbody;

    $result = $mail->send();
    return $result;

}
catch (Exception $e) {
    $result = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

