<?php
/*
 * types
 * 1 - plain text with commas & dots
 * 2 - email
 * 3 - html with allowed tags p, span, ul, li, br
 * 4 - phone
 */

function cleanText($string, $type=1) {
    switch ($type) {
        case 1:
            $result = strip_tags($string);
            $result = preg_replace('/[^a-zA-Zа-яёА-ЯЁ0-9 ,.:_-]/ui', '', $result);
            break;
        case 2:
            $result = strip_tags($string);
            $result = preg_replace('/[^a-zA-Zа-яёА-ЯЁ0-9 .@_-]/ui', '', $result);
            break;
        case 3:
            $result = strip_tags($string, '<p><span><ul><li><br><b>');
            break;
        case 4:
            $result = strip_tags($string);
            $result = preg_replace('/[^0-9]/ui+-._', '', $result);
    }
    return $result;
}


function generated($method=1,$type=1,$length=24) {
    $string = "";
    $string1 = "";
    if ($method==1) {
        switch ($type) {
            case 1:
                $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
                break;
            case 2:
                $chars = '1234567890';
                break;
            case 3:
                $chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890';
                break;
            default:
                $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        }

        $count_chars = strlen($chars);

        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, ($count_chars-1));
            $string .= substr($chars, $rand, 1);
            $string1 .= $rand.".";
        }
    }
    elseif ($method==2) {
        $vowels = "eyuioa";
        $consonants = "qwrtpsdfghkzxcvbnm";
        $count_vowels = strlen($vowels);
        $count_consonants = strlen($consonants);

        $i = 1;
        do {
            if ($i % 2 == 0) {
                $count_chars =  $count_vowels;
                $chars = $vowels;
            }
            else {
                $count_chars =  $count_consonants;
                $chars = $consonants;
            }

            $rand = mt_rand(1, $count_chars);
            $string .= substr($chars, $rand, 1);
            $count_string = strlen($string);
            $i++;
        }
        while ($count_string < $length);
    }
//    unset($i);
//    unset($count_string);
//    unset($length);
    return $string;
}

function translitEnRu($str)
{
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", " " => "_"
    );
    return strtr($str,$tr);
}

function phoneSerialize($phone) {
    $_uPhone = strip_tags($phone);
    $_uPhone = preg_replace('/[^0-9]/ui', '', $_uPhone);

    if (strlen($_uPhone) == 11 & strpos($_uPhone,"8") === 0 || strpos($_uPhone,"7") === 0) {
        $_uPhone = substr_replace($_uPhone,'',0,1);
    }

    if (strlen($_uPhone) == 10) {
        return $_uPhone;
    }
    else {
        return false;
    }
}