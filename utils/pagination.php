<?php
function getPagination($currentPage,$totalPages,$directory) {
    $pagination = '';
    if ($totalPages == 1) { $pagination = ""; }
    elseif ($totalPages > 1 && $totalPages < 6) {
        $pagination = "<nav aria-label='Page Navigation'><ul class='list-inline'>";
        $a = 1;
        do {
            if ($a == $currentPage) $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>{$a}</a></li>";
            else $pagination .= "<li  class='list-inline-item g-hidden-sm-down'><a  class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$a}'>{$a}</a></li>";
            $a++;

        } while ($a <= $totalPages);

        $pagination .= "</ul></nav>";
    }
    elseif ($totalPages > 6) {
        $activePageClass[$currentPage] = "class='active'";
        $prevPage = $currentPage - 1;
        $preprevPage = $currentPage - 2;
        $nextPage = $currentPage + 1;

        $lastPage = $totalPages;
        $prelastPage = $totalPages - 1;
        $preprelastPage = $totalPages - 2;

        $pagination = "<nav aria-label='Page Navigation'><ul class='list-inline'>";
        $pagination .= '<li class="list-inline-item"><a class="u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-13" href="?page='.$prevPage.'" aria-label="Предыдущая"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>';

        if ($currentPage == 1) {
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>1</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=2'>2</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=3'>3</a></li>";
            $pagination .= "<li class='list-inline-item disabled'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$preprelastPage}'>{$preprelastPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$prelastPage}'>{$prelastPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$lastPage}'>{$lastPage}</a></li>";
        }
        elseif ($currentPage == 2) {
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=1'>1</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>2</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=3'>3</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$preprelastPage}'>{$preprelastPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$prelastPage}'>{$prelastPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a  class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11'href='/{$directory}/?page={$lastPage}'>{$lastPage}</a></li>";
        }
        elseif ($nextPage == $totalPages) {
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=1'>1</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=2'>2</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=3'>3</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$prevPage}'>{$prevPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>{$currentPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$lastPage}'>{$lastPage}</a></li>";
        }
        elseif ($currentPage == $totalPages) {
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=1'>1</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=2'>2</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=3'>3</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$preprevPage}'>{$preprevPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$prevPage}'>{$prevPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>{$totalPages}</a></li>";
        }
        else {
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page=1'>1</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$prevPage}'>{$prevPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 u-pagination-v1-3--active g-rounded-50 g-pa-4-11'>{$currentPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$nextPage}'>{$nextPage}</a></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><span class='g-rounded-50 g-pa-4-11'>...</span></li>";
            $pagination .= "<li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-11' href='/{$directory}/?page={$lastPage}'>{$lastPage}</a></li>";
        }

        $pagination .= '<li class="list-inline-item"><a class="u-pagination-v1__item u-pagination-v1-3 g-rounded-50 g-pa-4-13" href="?page='.$nextPage.'" aria-label="Следу"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>';
        $pagination .= "</ul></nav>";

    }

    return $pagination;

}