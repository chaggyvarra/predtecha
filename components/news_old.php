<?php
if (!empty($_REQUEST['id'])) {
    $newsId = $_REQUEST['id'];
    $params = array("id" => (int)$newsId);
    $cNews = new Church\News\Get();
    $newsContent = $cNews->run($params);

    $smarty->assign('news_item',$newsContent);

    $pageTemplate = "templates/news/item.tpl";
}
else {
    $cNews = new Church\News\Getlist();
    $cPage = !$_REQUEST['page'] ? 1 : $_REQUEST['page'];
    $params = array("itemsOnPage" => 2, "currentPage" => $cPage);
    $newsContent = $cNews->run($params);

    if ($newsContent['pages'] > 0) {
        $cPage = !$_REQUEST['page'] ? 1 : $_REQUEST['page'];
        $pagination = getPagination($cPage, $newsContent['pages'], "news");
        $smarty->assign("pagination",$pagination);
    }
echo $cPage;

    $smarty->assign('news',$newsContent['items']);
    $pageTemplate = "templates/news/list.tpl";
}



