<?php
$month = date("n");

$scheduleJson = file_get_contents("data/schedules/2.json");
$scheduleArr = json_decode($scheduleJson,true);

$monthName = $scheduleArr['monthName']." ".date("Y");

$content = "";
foreach ($scheduleArr['days'] as $key => $value) {
    $day = $key;
    $weekDay = $value['weekday'];

        $i = 0;
        foreach ($value['hours'] as $k => $v) {
            $services[$i]['time'] = $k;
            $services[$i]['service'] = $v;
            $i++;
        }

    $rowspan = count($value['hours']) > 1 ? "rowspan=".count($value['hours']) : "";
    $class = $weekDay == "ВС" ? ' class="g-color-red g-font-weight-700"' : '';
    $hday = isset($value['hday']) & $value['hday'] == 1 ? ' class="g-color-red g-font-weight-700"' : '';
    $content .= "<tr ".$hday.">";
    $content .= "<td ".$rowspan." ".$class.">".$day."<br>".$weekDay." </td>";

    $content .= "<td>".$services[0]['time']."</td>";
    $content .= "<td width='33%'>".$services[0]['service']."</td>";

    $content .= "<td>".$value['col2']." </td>";
    $content .= "</tr>";

    if (count($value['hours']) > 1) {
        $i = 1;
        while ($i < count($value['hours'])) {
            $content .= "<tr ".$hday.">";

            $content .= "<td style='border: 0;'>".$services[$i]['time']."</td>";
            $content .= "<td style='border: 0;'>".$services[$i]['service']."</td>";

            $content .= "</tr>";
            $i++;
        }


    unset($services);
    }

}






//$pageTemplate = "templates/service-schedule/list.tpl";
$pageTemplate = "templates/service-schedule/skeletonjson.php";