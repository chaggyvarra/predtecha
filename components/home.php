<?php
$source = file_get_contents("http://script.days.ru/php.php?para=1&feofan=1&encoding=u&name=0&trop=0&icon=0");
$rowData=unserialize(stripslashes($source));

$smarty->assign("calendar", $rowData);

$Atrapeza_img = explode("src=\"", $rowData['trapeza_img']);
$AAtrapeza_img = explode("\"",$Atrapeza_img[1]);
$smarty->assign("calendar_trapeza_img", $AAtrapeza_img[0]);

$Adayicon = explode("src=\"", $rowData['dayicon']);
$AAdayicon = explode("\"",$Adayicon[1]);
$smarty->assign("calendar_dayicon", $AAdayicon[0]);

//get services
$today = date('j');
$currentHour = date('G');
//$today = "31";
$scheduleJson = file_get_contents("data/schedules/2.json");
$scheduleArr = json_decode($scheduleJson,true);
$serviceToday = "";

if (array_key_exists($today, $scheduleArr['days']) && $currentHour < 17) {
    $serviceToday .= "<h4>Богослужения сегодня в храме</h4>";
    $serviceToday .= '<div class="table-responsive"><table class="table"><tbody>';


    $weekDay = $scheduleArr['days'][$today]['weekday'];

    $i = 0;
    foreach ($scheduleArr['days'][$today]['hours'] as $k => $v) {
        $services[$i]['time'] = $k;
        $services[$i]['service'] = $v;
        $i++;
    }

    $rowspan = count($scheduleArr['days'][$today]['hours']) > 1 ? "rowspan=".count($scheduleArr['days'][$today]['hours']) : "";
    $class = $weekDay == "ВС" ? ' class="g-color-red g-font-weight-700"' : '';
    $content .= "<tr>";
    $content .= "<td ".$rowspan." ".$class.">".$today."<br>".$weekDay." </td>";

    $content .= "<td>".$services[0]['time']."</td>";
    $content .= "<td width='33%'>".$services[0]['service']."</td>";

    $content .= "<td>".$scheduleArr['days'][$today]['col2']." </td>";
    $content .= "</tr>";

    if (count($scheduleArr['days'][$today]['hours']) > 1) {
        $content .= "<tr>";

        $content .= "<td style='border: 0;'>".$services[1]['time']."</td>";
        $content .= "<td style='border: 0;'>".$services[1]['service']."</td>";

        $content .= "</tr>";
    }
    unset($services);

    $serviceToday .= $content;

    $serviceToday .= '</tbody></table></div>';

}
else {

   // $day = (int)$today > 31 ? 0 : (int)$today;
    $day = $today;

   // var_dump($day);exit;

    $stop = 1;

    for ($i = 1; $i < 31; $i++) {
        $nextday = (int)$day + $i;
        if (array_key_exists($nextday, $scheduleArr['days'])) {
            $stop = 0;
            break;
        }
    }

    if ($stop === 1) {
        $serviceToday = "";
    }
    else {
        $serviceToday = '<h4>Ближайшие богослужения в храме</h4>';
        $serviceToday .= '<div class="table-responsive"><table class="table"><tbody>';


        $weekDay = $scheduleArr['days'][$nextday]['weekday'];

        $i = 0;
        foreach ($scheduleArr['days'][$nextday]['hours'] as $k => $v) {
            $services[$i]['time'] = $k;
            $services[$i]['service'] = $v;
            $i++;
        }

        $rowspan = count($scheduleArr['days'][$nextday]['hours']) > 1 ? "rowspan=".count($scheduleArr['days'][$today]['hours']) : "";
        $class = $weekDay == "ВС" ? ' class="g-color-red g-font-weight-700"' : '';
        $hday = isset($value['hday']) & $scheduleArr['days'][$nextday]['hday'] == 1 ? ' class="g-color-red g-font-weight-700"' : '';
        $content .= "<tr ".$hday.">";
        $content .= "<td ".$rowspan." ".$class.">".$nextday."<br>".$weekDay." </td>";

        $content .= "<td>".$services[0]['time']."</td>";
        $content .= "<td width='33%'>".$services[0]['service']."</td>";

        $content .= "<td>".$scheduleArr['days'][$nextday]['col2']." </td>";
        $content .= "</tr>";

        if (count($scheduleArr['days'][$nextday]['hours']) > 1) {
            $content .= "<tr ".$hday.">";

            $content .= "<td style='border: 0;'>".$services[1]['time']."</td>";
            $content .= "<td style='border: 0;'>".$services[1]['service']."</td>";

            $content .= "</tr>";
        }
        unset($services);

        $serviceToday .= $content;

        $serviceToday .= '</tbody></table></div>';
    }

}



//get news
$newsJson = file_get_contents("data/news/news.json");
$aNews = json_decode($newsJson,true);

$i = 1;
foreach ($aNews as $k=>$v) {




        $id = $k;
        $title = $v['title'];
        $anounce = $v['anounce'];
        $date = $v['date'];
        $cover = !empty($v['cover']) ? $v['cover'] : "/assets/img/logo/logoFull.png";

        $newsContent .= '<div class="col-lg-6"> <div class="row g-mx-5--sm g-mb-30">';
        $newsContent .= '<div class="col-sm-5 g-px-0--sm"><div class="g-height-150 g-bg-size-cover" style="background-image: url('.$cover.');"></div></div>';
        $newsContent .= '<div class="col-sm-7 g-px-0--sm"><div class="u-info-v1-1 g-height-150 g-bg-gray-light-v5 g-bg-gray-light-gradient-v1--after g-pa-10"> <h3 class="h6 g-font-weight-600 text-uppercase small g-pt-2"><a class="u-link-v5 g-color-black g-color-primary--hover" href="/news/?id='.$id.'">'.$title.'</a></h3> <p class="small g-color-primary"><time>'.$date.'</time></p><p class="small">'.$anounce.'</p></div></div>';

        $newsContent .= '</div></div>';

  if ($i == 2) break;

    $i++;


}

//get ads
$adsJson = file_get_contents("data/ads/ads.json");
$aAds = json_decode($adsJson,true);
//for left column
$adsData = $aAds['leftcol'];
$uToday = time();
$adsContent = "";
foreach ($adsData as $v) {
    if ($uToday > $v['start'] && $uToday < $v['end']) {
        $adsPic = $v['pic'];
        $adsLink = $v['link'];
        $adsTarget = $v['target'];
        $adsContent .= "<div class='g-mt-10'>";
        $adsContent .= !empty($adsLink) ? "<a href=".$adsLink." target='".$adsTarget."'>" : "";
        $adsContent .= "<img src=".$adsPic." class='img-fluid'>";
        $adsContent .= !empty($adsLink) ? "</a>" : "";
        $adsContent .= "</div>";
    }
}





//$pageTemplate = "templates/home/home.tpl";
$pageTemplate = "templates/home/main.php";