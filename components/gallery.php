<?php
function search_img($path){
    $pics=[];
    foreach (glob($path."*.{jpg,png,gif}", GLOB_BRACE) as $filename)
    {
        $pics[] = $filename;
    }
    return $pics;
}

$pics = search_img("./assets/pic/gallery/full/");

foreach ($pics as $pic) {
    $pictures[] = array_pop(explode("/",$pic));
}

asort($pictures);
$total = count($pictures);
$itemsOnPage = 12;
$totalPages = ceil($total/$itemsOnPage);
$currentPage = !$_GET['page'] ? 1 : $_GET['page'];
$offset = ($currentPage - 1)*$itemsOnPage;

$picItems = array_slice($pictures,$offset,$itemsOnPage);


$smarty->assign("pics", $picItems);

if ($totalPages > 1) {
$pagination = getPagination((int)$currentPage, $totalPages, "gallery");
$smarty->assign("pagination", $pagination);
}

$pageTemplate = "templates/gallery/list.tpl";