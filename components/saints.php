<?php
$page = $pages[1];

$rightColumnPage = !empty($page) ? "templates/saints/{$page}.php" : "templates/saints/main.php";

if (file_exists($rightColumnPage) == false)
    $rightColumnPage = "templates/saints/main.php";

$pageTemplate = "templates/saints/skeleton.php";