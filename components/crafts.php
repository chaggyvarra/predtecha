<?php
$page = $pages[1];

$rightColumnPage = !empty($page) ? "templates/crafts/{$page}.php" : "templates/crafts/main.php";

if (file_exists($rightColumnPage) == false)
    $rightColumnPage = "templates/crafts/main.php";

$pageTemplate = "templates/crafts/skeleton.php";
