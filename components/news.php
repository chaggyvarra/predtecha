<?php
//get full list of news
$newsJson = file_get_contents("data/news/news.json");
$aNews = json_decode($newsJson,true);

if (isset($_REQUEST['id'])) {
    $newsItem = $aNews[$_REQUEST['id']];
    $smarty->assign("news_item", $newsItem);

    $pageTemplate = "templates/news/item.tpl";

}
else {
    $totalItems =  count($aNews);
    $itemsOnPage = 5;
    $totalPages = $totalItems/$itemsOnPage;

    $currentPage = !$_REQUEST['page'] ? 0 : $_REQUEST['page'] - 1;

    $aNews = array_chunk($aNews, $itemsOnPage, true );

    $i = 0;
    $newsItem = [];
    foreach ($aNews[$currentPage] as $k => $v) {

        $newsItem[$i]['id'] = $k;
        $newsItem[$i]['title'] = $v['title'];
        $newsItem[$i]['anounce'] = $v['anounce'];
        $newsItem[$i]['date'] = $v['date'];
        $newsItem[$i]['cover'] = !empty($v['cover']) ? $v['cover'] : "/assets/img/logo/logoFull.png";
        $newsItem[$i]['pics'] = count($v['pics']) > 0 ? $v['pics'] : 0;
        $i++;
    }


    if ($totalPages > 0) {
        $cPage = (int)$currentPage + 1;
        $pagination = getPagination($cPage, ceil($totalPages), "news");
        $smarty->assign("pagination",$pagination);

    }

    $smarty->assign("news", $newsItem);
    $pageTemplate = "templates/news/list.tpl";
}








