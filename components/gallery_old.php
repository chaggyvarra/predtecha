<?php
$dir = opendir('./assets/pic/gallery/');
$count = 0;
while($file = readdir($dir)){
    if($file == '.' || $file == '..' || is_dir('./assets/pic/gallery/' . $file)){
        continue;
    }
    $count++;
}
if ($count > 0) {
    $itemsOnPage = 12;
    $totalPages = $count/$itemsOnPage;
    $currentPage = !$_GET['page'] ? 1 : $_GET['page'];

    $imgName = 1;
    if ($_GET['page'] > 1) $imgName = ($imgName + $itemsOnPage*$currentPage) - $itemsOnPage;

    $counter = 1;
    $stop = $itemsOnPage;
    $isLastPage = $currentPage == ceil($totalPages) ? true : false;
    if ($isLastPage) $stop = $count - floor($totalPages)*$itemsOnPage;

    $content = '';
    do {
        $content .= "<div class='col-md-2 g-mb-30--md'>
        <div class='g-brd-around g-brd-gray-light-v4--hover'>
            <a class='js-fancybox d-block u-block-hover u-block-hover--scale-down' href='javascript:;' data-fancybox='lightbox-gallery--17' data-src='/assets/pic/gallery/$imgName.jpg' data-animate-in='bounceInDown' data-animate-out='bounceOutDown' data-speed='1000' data-overlay-blur-bg='true' data-caption='Lightbox Gallery'>
                <img class='img-fluid u-block-hover__main--grayscale u-block-hover__img' src='/assets/pic/gallery/thumb/$imgName.jpg'>
            </a>
        </div>
    </div>";
        $imgName++;
        $counter++;
    }
    while ($counter <= $stop);

    $pagination = getPagination((int)$currentPage, ceil($totalPages), $pages[0]);


}




$pageTemplate = "templates/gallery/skeleton.php";