<?php

namespace Church\Schedule;

use Church\Db\Database;
use Church\Utils\Strings;

class Get {

    public function run($params) {
        $db = new Database();
        $month = isset($params['month']) ? $params['month'] : date("n");

        $query = "select * from `schedule` where `month` = '{$month}' order by day, time desc";

        $rawSchedule = $db->query($query, "select");

        $i = 0;
        foreach ($rawSchedule as $day) {
            $weekDay = date("N", mktime(0, 0, 0, $day['month'], $day['day'], date("Y")));
            $rawSchedule[$i]['weekDay'] = $weekDay;
            $rawSchedule[$i]['weekDay_txt'] = Strings::transcodeDate("day",$weekDay);
            $i++;

        }


        return $rawSchedule;

    }

} 