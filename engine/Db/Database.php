<?php
namespace Church\Db;

class Database {
    protected $connection;


    public function __construct() {

        $host = "localhost";
        $user = "church_admin";
        $password = "1234567890Q";
        $db_name = "church";

        $this->connection = new \mysqli($host, $user, $password, $db_name);

        $this->query("SET NAMES UTF8","set");

        if( !$this->connection ) {
            throw new \Exception('Could not connect to DB ', 155);
        }
    }

    public function query($sql,$action){
        if ( !$this->connection ){
            return false;
        }

        $result = $this->connection->query($sql);

        if ( mysqli_error($this->connection) ){
            throw new \Exception(mysqli_error($this->connection));
        }

        if ( is_bool($result) && $action == "insert" ){
            return $result = $this->connection->insert_id;
        }

        if ( is_bool($result) ){
            return $result;
        }

        $data = array();
        while( $row = mysqli_fetch_assoc($result) ){
            $data[] = $row;
        }

        mysqli_free_result($result);

        return $data;
    }

    public function escape($str){
        return mysqli_escape_string($this->connection, $str);
    }
} 