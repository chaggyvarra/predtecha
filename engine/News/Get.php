<?php

namespace Church\News;

use Church\Db\Database;

class Get {
    public function run($params) {
        $result = [];
        $db = new DataBase();

        $id = (int)$params['id'];


        $query = "select news.* from `news` where news.id = '{$id}'";
        $newsBody = $db->query($query, "select");

        $query = "select url from `pictures` where `itemId` = '{$id}' and `type` = 'news'";
        $newsPictures = $db->query($query, "select");

        $result = $newsBody[0];
        $aDate = explode(" ", $result['date']);
        $aDate = explode("-", $aDate[0]);
        //$aDate = $aDate[2].".".$aDate[1].".".$aDate[0];
        $result['date'] = $aDate[2].".".$aDate[1].".".$aDate[0];

        foreach ($newsPictures as $picture) {
            $result['pics'][] = "/uploads/".$picture['url'];
        }
        if (count($newsPictures) == 0) $result[0]['pics'][] = "/assets/default.jpg";

        return $result;
    }

} 