<?php

namespace Church\News;

use Church\Db\Database;

class Getlist {

    public function run($params) {
        $result = [];
        $db = new DataBase();
        $itemsOnPage = (isset($params['itemsOnPage'])) ? (int)$params['itemsOnPage'] : 10;
        $page = (isset($params['currentPage'])) ? (int)$params['currentPage'] - 1 : 0;

        $offset = $page*$itemsOnPage;

       // $query = "select news.* from `news` order by `date` desc limit {$offset}, {$itemsOnPage}";
        $query = "select news.* from `news` order by `date` desc";
        $newsBody = $db->query($query, "select");



        $i=0;
        foreach ($newsBody as $new) {
            $id = $new['id'];
            $query = "select url from `pictures` where `itemId` = '{$id}' and `type` = 'news'";
            $newsPictures = $db->query($query, "select");
            foreach ($newsPictures as $picture) {
                $newsBody[$i]['pics'][] = "/uploads/".$picture['url'];
            }
            if (count($newsPictures) == 0) $newsBody[$i]['pics'][] = "/assets/pic/default.jpg";

            $aDate = explode(" ", $new['date']);
            $aDate = explode("-", $aDate[0]);
            //$aDate = $aDate[2].".".$aDate[1].".".$aDate[0];
            $newsBody[$i]['date'] = $aDate[2].".".$aDate[1].".".$aDate[0];

            $aText = explode(".", $new['text']);
            $newsBody[$i]['short'] = $aText[0]."...";

            $i++;
        }

        $result['total'] = count($newsBody);
        $result['pages'] = ceil(count($newsBody)/$itemsOnPage);
        $result['items'] = array_slice($newsBody,$offset,$itemsOnPage);




        return $result;

    }

} 