<?php

//$str = 'sd34 w (3r3w w 33w 3w w[]w33*0.,/])3kl3f';
//
//preg_match('/\((.+)\)/', $str, $m);
//
//echo $m[1];exit;


session_start();
require_once '../vendor/autoload.php';
include("config/Settings.php");

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url = ltrim($url, "/");
$url = rtrim($url, "/");
$pages = explode("/", $url);

$jsonStr = file_get_contents("php://input"); //read the HTTP body.
$params = json_decode($jsonStr, true);


if (!$params)
    $params = $_REQUEST;

$aNewInstanceName = array();
foreach(explode('.', $pages[1]) as $sItem)
    $aNewInstanceName[] = ucfirst(mb_convert_case($sItem, MB_CASE_LOWER));

$sClassName = '\\Church\\' .implode('\\', $aNewInstanceName);

if(!class_exists($sClassName))
    die('Oops');

$methodInstance = new $sClassName();
try {
    $answer = $methodInstance->run($params);
    $answer = array('result' => $answer);
}catch (\Exception $e) {
//    $eCode = $e->getCode();
//    $error = Errors::getError($eCode);
//    $answer = array ("error" => array(
//        "code" => $eCode,
//        "message" => $error
//    ));


    $answer = array('error' => array('code' => $e->getCode(), 'text' => $e->getMessage()));
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,OPTIONS");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Content-Type: application/json; charset=utf-8");
echo json_encode($answer);
//var_dump((array) $answer);
unset($params);
exit;