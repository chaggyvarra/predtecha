<?php

namespace Church\Login;

use Church\Utils\Strings;
use Church\Db\Database;

class Login {

    public function run($params) {
        $db = new Database();

        $uLogin = Strings::cleanText($params['login'],2);
        $uPwd = md5($params['password']);

        if (empty($uLogin) || empty($uPwd)) {
            throw new \Exception('Не указан логин или пароль', 1);
        }

        $sql = "select * from `users` where users.`login` = '$uLogin' limit 0,1";
        $sUserData = $db->query($sql,"select");

        if (empty($sUserData)) {
            throw new \Exception('Нет такого пользователя', 2);
        }

        $result = $sUserData[0]['password'] != $uPwd ? false : true;

        return $result;


    }
} 