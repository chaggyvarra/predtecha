<?php
namespace Church\Utils;

class Strings {
    public static function cleanText($string, $type=1) {
        switch ($type) {
            case 1:
                $result = strip_tags($string);
                $result = preg_replace('/[^a-zA-Zа-яёА-ЯЁ0-9 ,.:_-]/ui', '', $result);
                break;
            case 2:
                $result = strip_tags($string);
                $result = preg_replace('/[^a-zA-Zа-яёА-ЯЁ0-9 .@_-]/ui', '', $result);
                break;
            case 3:
                $result = strip_tags($string, '<p><span><ul><li><br><b><img>');
                break;
            case 4:
                $result = strip_tags($string);
                $result = preg_replace('/[^0-9+]/ui', '', $result);
                break;
            case 5:
                $result = strip_tags($string);
                $result = preg_replace('/[^a-zA-Z0-9 .:_-]/ui', '', $result);
                break;
        }
        return $result;
    }

    public static function generated($lenght = 24,$type=1) {
        switch ($type) {
            case 1:
                $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
                break;
            case 2:
                $chars = '1234567890';
                break;
            default:
                $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
                break;
        }

        $count_chars = strlen($chars);
        $string = '';

        for ($i = 0; $i < $lenght; $i++) {
            $rand = mt_rand(1, $count_chars);
            $string .= substr($chars, $rand, 1);
        }
        return $string;
    }

    public static function translitRuEn($str)
    {
        $tr = array(
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
            "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
            "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
            "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", " " => "_"
        );
        return strtr($str,$tr);
    }

    public static function getHash($str) {
        return crypt($str, '$2y$07$rxDR2fK6UTSCxz13YXxjxs');
    }

    public static function hideEmail($email) {
        $parts = explode('@', $email);
        return substr($parts[0], 0, min(1, strlen($parts[0])-1)) . str_repeat('*', max(1, strlen($parts[0]) - 1)) . '@' . $parts[1]; }

    public static function transcodeDate($type,$value) {
        $__MONTHS = array(
            1 => "январь",
            2 => "февраль",
            3 => "март",
            4 => "апрель",
            5 => "май",
            6 => "июнь",
            7 => "июль",
            8 => "август",
            9 => "сентябрь",
            10 => "октябрь",
            11 => "ноябрь",
            12 => "декабрь"
        );

        $__DAYS = array(
            1 => array("short" => "пн", "full" => "понедельник"),
            2 => array("short" => "вт", "full" => "вторник"),
            3 => array("short" => "ср", "full" => "среда"),
            4 => array("short" => "чт", "full" => "четверг"),
            5 => array("short" => "пт", "full" => "пятница"),
            6 => array("short" => "сб", "full" => "суббота"),
            7 => array("short" => "вс", "full" => "воскресенье")
        );
        if ($type == "month") return $__MONTHS[$value];
        elseif ($type == "day") return $__DAYS[$value];
        else return null;
    }
}