

    /* =================================
     ===  CONTACT FORM               ====
     =================================== */
    $("#sendMessage").submit(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var name = $("#" + fs + "-name").val();
        //var email = $("#" + fs + "-email").val();
        var message =  $("#" + fs + "-message").val();
        var message_type = $("input[name=" + fs + "-type]:checked").val();
        var dataString = 'name=' + name + '&message=' + message + '&mtype=' + message_type;

        $.ajax({
            type: "POST",
            url: "/utils/send_mail.php",
            data: dataString,
            success: function () {
                    $('.successContent').fadeIn(1000);
                    $('.errorContent').fadeOut(500);
                    $("#" + fs + "-name").val('');
                    $("#" + fs + "-message").val('');
                    setTimeout("$('.successContent').fadeOut(500)", 10000);
                }});


//        if ((message.length > 1) && (name.length > 1)) {
//            $.ajax({
//                type: "POST",
//                url: "/utils/send_mail.php",
//                data: dataString,
//                success: function () {
//                    $('.successContent').fadeIn(1000);
//                    $('.errorContent').fadeOut(500);
//
//                    $("#" + fs + "-name").val('');
//                    $("#" + fs + "-message").val('');
//                }
//            });
//        }
//        else {
//            $('.errorContent').fadeIn(1000);
//            $('.successContent').fadeOut(500);
//        }

        //return false;
    });

